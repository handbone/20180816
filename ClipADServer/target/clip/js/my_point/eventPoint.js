var myPoint = {
    //----------------------------
    // InfoIndex 객체
    //----------------------------
    name: 'myPoint',

    /**URL 지정**/
    functionURL1: "getUserCi.do",
    functionURL2: "getPoint.do",
    functionURL3: "getPointHistory.do",
    functionURL4: "getBannerInfo.do",
    
    /**리스트 페이징 변수**/
    initSaveList : null,
    initUseList : null,
    
    preSaveEnd : 0,
    preUseEnd : 0,
    saveListLength : 0,
    useListLength : 0,

    /**os 타입 변수**/
    osType : null,
    
    init: function () {
        this.beforeBind();
        this.bind();
        this.afterBind();
    },

    beforeBind: function () {
    	
    },

    bind: function () {
    	/**os type 체크**/
    	var userAgent = navigator.userAgent.toLowerCase();
    	
    	if ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1)	|| (userAgent.search("ipad") > -1)){
    		myPoint.osType = "ios";
    	}else{
    		myPoint.osType = "android";
    	}
    	
    	/**2배 이미지 =========================================**/
    	var now = new Date();
//        now = new Date("9/22/2016 21:50:00");
        var startDate = new Date("9/22/2016 09:00:00");
        var endDate = new Date("11/30/2016 23:55:00");
        
        if (startDate <= now && now <= endDate) {
           $("#doublePoint").show()
        }
    	/**==================================================**/
        
    	var cust_id = $("#cust_id").val();
    	var ctn = $("#ctn").val();
    	var gaid = $("#gaid").val();
    	var mid = $("#mid").val();
    	
    	var user_ci = $("#user_ci").val();
    	
    	var offerwall = $("#offerwall").val();
    	
    	/**기프티쇼 적립 버트 on off=================================**/
    	if(offerwall == "on"){
    		$("#moreSavingBtn").css("visibility","visible");
    	}
    	/**==================================================**/
     	
    	/**아이폰에서 잠금화면 적립버튼 제외 ==========================**/
    	if(myPoint.osType == "ios"){
    		$("#lockscreen_li").hide();
    		$(".pointBtnDiv").css("padding-top","0px");
    		$(".pointBtnDiv").css("text-align","center");
            $("#moreSavingBtn").css("display","none");
         //   $("#moreSavingBtn").css("visibility","none");
			$("#buyGiftiShowBtn").css("float","none");
			$(".bannerDiv").css("padding-top","10px");
    	}
    	/**==================================================**/
    	
    	
    	
    	if((cust_id == null || cust_id == '') || (ctn == null || ctn == '') || (gaid == null || gaid == '')){
    		alert("필수 정보가 없습니다. 종료 후 다시 시도해 주세요.");
    		if(myPoint.osType == "ios"){
        		window.location="jscall://closePoint";
        	}else{
        		window.ClipPoint.closePoint();        		
        	}
    		return;
    	}else{
    		/**베너 정보 조회**/
    		myPoint.fn.getBannerInfo();
    		
    		if(user_ci == null || user_ci == ""){
    			/**user ci 조회**/
    			myPoint.fn.getUserCi();
    		}else{ 	
    			/**잔여 포인트 조회**/
		    	myPoint.fn.getPoint();
    		}
    		
    		/*user ci 조회*/
//	    	myPoint.fn.getUserCi();
	    	
	    	
	    	/** 조회 기준일 =========================================**/
	    	$("#selectDate").empty();
	    	$("#selectDate").append(myPoint.fn.getTimeStamp());
	    	/**==================================================**/
	    	
	    	/**오퍼월 직링크 안드로이드일 때만============================**/
	    	if (myPoint.osType == "android"){
	    		
	    		
				var today = new Date();
				var offerwallStartDate = new Date("11/15/2016 09:00:00");
				var offerwallEndDate = new Date("12/31/2016 23:55:00");
				
				 if (offerwallStartDate <= now && now <= offerwallEndDate) {
					 $("#bannerDiv2").show();
					 
					 $(".banner_slide2").empty();
					 
					 var shuffleArray = [0,1,2,3,4];
					 shuffleArray = myPoint.fn.shuffle(shuffleArray);
					 
					 var html = "";
					 var imageSrc = "../images/mypoint/banner/pointmain_banner";		
					 
					 for(var i in shuffleArray){
						 	
	        				html += "<li style=\"width:100%;\">";
	            			html += "<img src=\""+imageSrc+shuffleArray[i]+".png\" style=\"width:100%;\" ";
	            			html += "alt=\"오퍼월이벤트베너\" title=\"새창열림\" onclick=\"javascript:myPoint.fn.moreSaving();\"/>";
	            			html += "</li>";
	            			
		        		}
					 
					 $(".banner_slide2").append(html);
					 myPoint.fn.startSlider2();
					 
				}
				 
				//락스크린 사용여부 
				 myPoint.fn.getLockScreenOnOff();
				 
	    	}
	    	/**==================================================**/
    	}
    	
    },

    afterBind: function () {
    	
    	/**==즉시적립하기 페이지 노출==**/
    	if (myPoint.osType == "android"){
    		setTimeout("myPoint.fn.moreSaving();",2000);
    	}
    	
    	myPoint.moreList.scroll();
    	
    },

    // ----------------------------------
    // 처리 메서드가 정의된 객체
    // ----------------------------------
    fn: {
    	/**랜덤함수**/
    	shuffle : function(d){
    		   for(var c = d.length - 1; c > 0; c--){
    		       var b = Math.floor(Math.random() * (c + 1));
    		       var a = d[c]; d[c] = d[b]; d[b] = a;
    		   }
    		   return d
    	},
    	
    	
    	/** 조회 기준일 **/
    	getTimeStamp : function(){
    		var d = new Date();

    		  var s =
    			  myPoint.fn.leadingZeros(d.getFullYear(), 4) + '-' +
    			  myPoint.fn.leadingZeros(d.getMonth() + 1, 2) + '-' +
    			  myPoint.fn.leadingZeros(d.getDate(), 2) + ' ' +

    			  myPoint.fn.leadingZeros(d.getHours(), 2) + ':' +
    			  myPoint.fn.leadingZeros(d.getMinutes(), 2);

    		  return s;
    	},
    	
    	leadingZeros : function(n, digits) {
    		  var zero = '';
    		  n = n.toString();

    		  if (n.length < digits) {
    		    for (i = 0; i < digits - n.length; i++)
    		      zero += '0';
    		  }
    		  return zero + n;
		},
		
    	/**콤마 넣기**/
        addComma : function (n) {
            var reg = /(^[+-]?\d+)(\d{3})/;
            n += '';

            while (reg.test(n))
                n = n.replace(reg, '$1' + ',' + '$2');
            return n;
        },
		
    	/** user ci 조회 **/
		getUserCi : function () {
    		var cust_id = $("#cust_id").val();
    		var ctn = $("#ctn").val();
    		var gaid = $("#gaid").val();
    		var mid = $("#mid").val();
    		var param = {
    				"cust_id"				: cust_id,
    				"ctn"				: ctn,
    				"gaid"				: gaid,
    				"mid"				: mid
    		};
    		
			var	opts = {
					url		: myPoint.functionURL1,
					data	: param,
					type	: "post",
					sendDataType	: "json",
					success	: function(resJSON, resCode) {
						console.log(resJSON);
						if (resCode== "success") {
							var user_ci = resJSON.data.user_ci;
							
							if(user_ci == "" || user_ci == null){
								alert("클립포인트 조회중 오류가 발생하였습니다.");
								if(myPoint.osType == "ios"){
									window.location="jscall://closePoint";
								}else{
									window.ClipPoint.closePoint();        		
								}
							}else{
								$("#user_ci").val(user_ci);
								$("#text_user_ci").val(user_ci);
								
								/*잔여 포인트 조회*/
						    	myPoint.fn.getPoint();
						    	
							}
							
						}else{	
							alert("오류가 발생하였습니다.");
		//					window.ClipPoint.closePoint();
						}
					}
					
				};
				common.http.ajax(opts);
    	},
    	
    	
    	
    	/** 잔여 포인트 조회 **/
    	
    	
    	getPoint : function () {
    		var user_ci = $("#user_ci").val();
    		var cust_id = $("#cust_id").val();
    		var ctn = $("#ctn").val();
    		var gaid = $("#gaid").val();
    		var mid = $("#mid").val();
    		var param = {
    				"user_ci"				: user_ci,
    				"cust_id"				: cust_id,
    				"ctn"				: ctn,
    				"gaid"				: gaid,
    				"mid"				: mid
    		};
			var	opts = {
					url		: myPoint.functionURL2,
					data	: param,
					type	: "post",
					sendDataType	: "json",
					success	: function(resJSON, resCode) {
						console.log(resJSON);
						if (resCode== "success") {
							var balance = myPoint.fn.addComma(resJSON.data.balance);
							if(balance == "-1"){
								alert("사용가능 포인트 조회중 오류가 발생하였습니다.\n 오른쪽 위 새로고침 버튼을 눌러 주세요.");
							}else{
								$("#mypoint").empty();
								$("#mypoint").append(balance);							
								
								/*point history 조회*/
						    	myPoint.fn.getPointHistory();
								
							}
						}else{	
							alert("오류가 발생하였습니다.");
						}
					}
					
				};
				common.http.ajax(opts);
    	},
    	
    	
    	
    	
    	
    	
    	/**포인트 이력 조회**/
    	
    	getPointHistory : function(){
    		var user_ci = $("#user_ci").val();
    		
    		var d = new Date();
    		var start_date = myPoint.fn.leadingZeros(d.getFullYear(), 4) + myPoint.fn.leadingZeros(d.getMonth() + -2, 2) + myPoint.fn.leadingZeros(d.getDate(), 2);
    		var end_date = myPoint.fn.leadingZeros(d.getFullYear(), 4) + myPoint.fn.leadingZeros(d.getMonth() + 1, 2) + myPoint.fn.leadingZeros(d.getDate(), 2);

    		var param = {
    				"user_ci"				: user_ci,
    				"start_date"          : start_date,
    				"end_date"          : end_date
    		};
			var	opts = {
					url		: myPoint.functionURL3,
					data	: param,
					type	: "post",
					sendDataType	: "json",
					success	: function(resJSON, resCode) {
						console.log(resJSON);
						
						//페이징에 사용하기 위해 넣어줌 
						myPoint.initSaveList = resJSON.data.saveList;
						myPoint.initUseList = resJSON.data.useList;
						myPoint.saveListLength = resJSON.data.saveList.length;
						myPoint.useListLength = resJSON.data.useList.length;
						
						if (resCode== "success") {
							if(resJSON.data.resResult == "failure"){
								alert("포인트 내역 조회중 오류가 발생하였습니다. \n 오른쪽 위 새로고침 버튼을 눌러 주세요.");
							}

							myPoint.fn.drawHistoryList();
							
						}else{	
							alert("오류가 발생하였습니다.");
						}
					}
					
				};
				common.http.ajax(opts);   		
    	},
    	
    	
    	
    	/**적립 포인트 이력 출력**/
    	
    	
    	drawHistoryList : function(){
    		
    		var saveHtml = "";    		
    		var saveList = myPoint.initSaveList;
    		var saveStartCnt;
    		
    		var useHtml = "";
    		var useList = myPoint.initUseList;
    		var useStartCnt;
    		
    		
//    		if(myPoint.preSaveEnd == 0){
//    			saveStartCnt = saveList.length -1;
//    		}else{
//    			saveStartCnt = myPoint.preSaveEnd -1;
//    		}
//    		var saveEndCnt = saveStartCnt - 30;
    		
    		if(myPoint.preSaveEnd == 0){
    			saveStartCnt = 0;
    		}else{
    			saveStartCnt = myPoint.preSaveEnd + 1;
    		}
    		var saveEndCnt = saveStartCnt + 30;
    		
    		
    		
    		if(myPoint.preUseEnd == 0){
    			useStartCnt = 0;
    		}else{
    			useStartCnt = myPoint.preUseEnd +1;
    		}
    		var useEndCnt = useStartCnt + 30;
    		
    		// 적립내역
    		if (saveList.length != 0) {
//    			saveHtml += "<li class=\"bdListLi\" style=\"text-align:center; line-height:3em;\">";
//    			saveHtml += "<span>적립 내역이 없습니다.</span>";
//    			saveHtml += "</li>";
//			}else{
				
        		if(myPoint.preSaveEnd == 0){
        			$("#bdSaveList").empty();    			
        		}
    			
				for(var i = saveStartCnt ; i <= saveEndCnt ; i++){
					if(i > (myPoint.saveListLength-1)){
						break;
					}
					var saveDescription = "";
					if(saveList[i].description == null){
						saveDescription = "기타 적립";
					}else{
						saveDescription = saveList[i].description;
					}
					saveHtml += "<li class=\"typeT1\">";
					saveHtml += "<span class=\"listTit pr85 pt7\">"+saveDescription+"</span>";
					saveHtml += "<span class=\"listName pr85 mb7\" >"+myPoint.fn.dataFormat(saveList[i].datetime)+"</span>";
					saveHtml += "<span class=\"listBtnB\">+"+myPoint.fn.addComma(saveList[i].point_value)+" ⓟ</span>";
					saveHtml += "</li>";					
				}				

			}
    		
    		
    		// 사용 내역
    		if (useList.length != 0) {
//    			useHtml += "<li class=\"bdListLi\" style=\"text-align:center; line-height:3em;\">";
//    			useHtml +="<span>사용 내역이 없습니다.</span>";
//    			useHtml +="</li>";
//			}else{
				
    			if(myPoint.preUseEnd == 0){
        			$("#bdUseList").empty();    			
        		}
        		
				for(var j = useStartCnt ; j <= useEndCnt ; j++){
					if(j > (myPoint.useListLength-1)){
						break;
					}
					var useDescription = "";
					if(useList[j].description == null){
						useDescription = "기타 적립";
					}else{
						useDescription = useList[j].description;
					}
					useHtml += "<li class=\"typeT1\">";
					useHtml += "<span class=\"listTit pr85 pt7\">"+useDescription+"</span>";
					useHtml += "<span class=\"listName pr85 mb7\" >"+myPoint.fn.dataFormat(useList[j].datetime)+"</span>";
					useHtml += "<span class=\"listBtnR\">-"+myPoint.fn.addComma(useList[j].point_value)+" ⓟ</span>";
					useHtml += "</li>";					
				}				

			}

    		myPoint.preSaveEnd = saveEndCnt
    		$("#bdSaveList").append(saveHtml);    		
    		
    		myPoint.preUseEnd = useEndCnt
    		$("#bdUseList").append(useHtml);
    		
    		
    	},
    	
    	
 
    	
    	/** 디비에서 베너 정보 조회**/
    	
    	
    	getBannerInfo : function (){
			var	opts = {
					url		: myPoint.functionURL4,
//					data	: param,
					type	: "post",
					sendDataType	: "json",
					success	: function(resJSON, resCode) {
						console.log(resJSON);
						myPoint.fn.drawBannerList(resJSON.data.bannerInfoList);
					}
					
				};
				common.http.ajax(opts);   		
    		
    	},
    	
    	
    	/**배너 그리기**/
    	
    	drawBannerList : function(bannerInfoList){
    		var imgPrePath = "../images/mypoint/banner/";
    		var html = "";
    		$(".banner_slide").empty();
    	 	
		var iC = 0;	
    		if(myPoint.osType == "ios"){
    			for(var i in bannerInfoList){
        			if("Y" == bannerInfoList[i].iosgbn ){
        				html += "<li style=\"width:100%;\">";
            			html += "<a href=\"javascript:myPoint.fn.moveUrl('"+bannerInfoList[i].linktype+"','"+bannerInfoList[i].linkurl+"')\">";
            			html += "<img src=\""+imgPrePath+bannerInfoList[i].bannerimage+"\" style=\"width:100%;\" alt=\""+bannerInfoList[i].bannername+"\" title=\"새창열림\" />";
            			html += "</a>";
            			html += "</li>";
				iC=iC+1;
        			}
        		}
        	}else{
        		for(var i in bannerInfoList){
        			if("Y" == bannerInfoList[i].androidgbn ){
        				html += "<li style=\"width:100%;\">";
            			html += "<a href=\"javascript:myPoint.fn.moveUrl('"+bannerInfoList[i].linktype+"','"+bannerInfoList[i].linkurl+"')\">";
            			html += "<img src=\""+imgPrePath+bannerInfoList[i].bannerimage+"\" style=\"width:100%;\" alt=\""+bannerInfoList[i].bannername+"\" title=\"새창열림\" />";
            			html += "</a>";
            			html += "</li>";
				iC=iC+1;
        			}
        		}		
        	}
    		
    		
    		$(".banner_slide").append(html);
    		
		if (iC > 1) {
    			myPoint.fn.startSlider();
    		}
    		
    	},
    	
    	moveUrl : function(linktype, linkurl){
    		
    		var paramId = "";
    		var startUrl = "";
    		var endUrl = "";
    		var resultUrl = "";
    		var paramVal = "";
    		
    		if(linktype == "http"){
    			
    			var chkStr = linkurl.indexOf('{');
    			
    			if(chkStr == -1){
    				location.href = linkurl;
    			}else{
    				var tempArray1 = linkurl.split('{');
    				var tempArray2 = tempArray1[1].split('}');
    				paramId = tempArray2[0];
    				startUrl = tempArray1[0];
    				endUrl = tempArray2[1];
    				
    				paramVal = $("#"+paramId).val();		
    				resultUrl = startUrl+encodeURIComponent(paramVal)+endUrl;
    				location.href = resultUrl;
    			}
    			
    		}else{
    			//스크립트 호출
    			linkurl;
    		}
    	},
    	
    	dataFormat : function(strDate){
    		
    		var resultDate = "";
    		var sYear = strDate.substring(0,4);
    		var sMonth = strDate.substring(4,6);
    		var sDay = strDate.substring(6,8);
    		
    		var sHour = strDate.substring(8,10);
    		var sMin = strDate.substring(10,12);
    		var sSec = strDate.substring(12,14);
    		
    		resultDate = sYear+"-"+sMonth+"-"+sDay+" "+sHour+":"+sMin+":"+sSec;
    		
    		return resultDate;
    	},
    	
    	
    	
    	/**락스크린 버튼 on/off 유무 확인**/
    	
    	
        getLockScreenOnOff : function(){
        	var on_off = window.ClipPoint.isLocker();
//        	var on_off = "0";
        	$("#lockScreen_on_off").val(on_off);
        	
        	if(on_off == "1"){
        		$("#lockScreenBtn").attr("src","../images/mypoint/img_lockscreen_on.png");
        	}else{
        		$("#lockScreenBtn").attr("src","../images/mypoint/img_lockscreen_off.png");
        	}
        	
        },
        
        
        showList : function(listName){
        	if(listName == "save"){
        		$("#saveTab").attr("src","../images/mypoint/img_saveTab_on.png");
        		$("#useTab").attr("src","../images/mypoint/img_useTab_off.png");
        		$("#bdSaveList").show();
        		$("#bdUseList").hide();
        	}else{
        		$("#useTab").attr("src","../images/mypoint/img_useTab_on.png");
        		$("#saveTab").attr("src","../images/mypoint/img_saveTab_off.png");
        		$("#bdUseList").show();
        		$("#bdSaveList").hide();
        	}
        },
        
        startSlider: function () {

            $('.banner_slide').bxSlider({
                mode: 'horizontal',
                pager: true,
                captions : true,
                controls :false,         
                auto  : false
            });

        },
        
        startSlider2: function () {

            $('.banner_slide2').bxSlider({
                mode: 'horizontal',
                pager: true,
                captions : true,
                controls :false,         
                auto  : false
            });

        },
        refresh : function(){
        	location.reload();
//		window.ClipPoint.reload();
        },
        
        backEvent : function(){
        	if(myPoint.osType == "ios"){
        		window.location="jscall://closePoint";
        	}else{
        		location.href = "KTolleh00114://"
//        		window.ClipPoint.closePoint();        		
        	}
        },
        
        lockScreenSwitch : function(){
			var on_off = $("#lockScreen_on_off").val();
			if(on_off == "1"){
				//on javascript 호출
				window.ClipPoint.offLocker();
				$("#lockScreen_on_off").val("0");
				$("#lockScreenBtn").attr("src","../images/mypoint/img_lockscreen_off.png");
			}else{
				//off javascript 호출
				window.ClipPoint.onLocker();
				$("#lockScreen_on_off").val("1");
				$("#lockScreenBtn").attr("src","../images/mypoint/img_lockscreen_on.png");					
			}
        },
        
        moreSaving : function(){
			window.ClipPoint.callOfferwall();
        },
        
        buyGiftiShow : function(){
        	if(myPoint.osType == "ios"){
        		window.location="jscall://callGiftishow";
        	}else{
        		window.ClipPoint.callGiftishow();        		
        	}
        }
        
    },
    
    
    moreList : {
    	scroll : function(){
    		$(window).scroll(function(){
    	        if  ($(window).scrollTop() == $(document).height() - $(window).height()){  
    	        	myPoint.fn.drawHistoryList();
    	        }  
    		});  
    	}
    }
};