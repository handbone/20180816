<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>포인트 적립 / 사용 페이지</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>

<script type="text/javascript">

var saveOrUse = function(){
	var cust_id = $("#cust_id option:selected").text();
	var desc = $("#desc option:selected").text();
	var action = $("#desc option:selected").val();
	var point = $("#point").val();
	var strAction = "";
	
	var params = "cust_id="+cust_id+"&action="+action+"&desc="+desc+"&point="+point;
	
    $.ajax({
        type: "POST",
        url: "saveOrUse.do",
        data: params,
        dataType: "Json",
        success: function (resData) {
            console.log(resData);
            if(action == "save"){
            	strAction = "적립";
            }else{
            	strAction = "사용";
            }
            if (resData.result == "success") {
            	alert("포인트"+strAction+"성공!!");
            }
            else{
            	alert("포인트"+strAction+"실패!!");
            }
            
        },
        beforeSend: function () {
//         	alert(this.data);
        },
        error: function (e) {
            console.log("error:" + e.responseText);
            alert("네트워크 오류가 발생하였습니다.");
        }
    });
}

</script>
<body>
<form id="mainForm" name="mainForm" action="">
</form>	
<h2>포인트 적립 / 사용 페이지</h2>

cust_id : 
<select id="cust_id">
<!-- 	<option value="">0jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">1jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">2jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">3jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">4jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">6jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">7jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">8jtXDc8/vDbcVNNKqAHsOg==</option>
	<option value="">9jtXDc8/vDbcVNNKqAHsOg==</option> -->
		<option value="">16551651</option>
		<option value="">16551652</option>
		<option value="">16551653</option>
		<option value="">16551654</option>
		<option value="">16551655</option>
	<!-- <option value="">16551656</option>
		<option value="">16551657</option>
		<option value="">16551658</option>
		<option value="">16551659</option> -->
</select>
<br/>
<br/>
Description : 
<select id="desc">
	<option value="save">락스크린테스트-포켓몬고 설치</option>
	<option value="save">즉시적립테스트-올레멤버십 등록</option>
	<option value="use">스타벅스 아메리카노</option>
</select>
<br/>
<br/>

point : <input type="text" id="point" value=""/><br/><br/>

<input type="button" id="save" value="확인" onclick="javascript:saveOrUse();">
<!-- <input type="button" id="save" value="적립" onclick="javascript:saveOrUse(this.id);"> -->
<!-- <input type="button" id="use" value="사용" onclick="javascript:saveOrUse(this.id);"> -->
</body>
</html>