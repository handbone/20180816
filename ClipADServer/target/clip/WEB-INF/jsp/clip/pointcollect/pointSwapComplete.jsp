<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">


</head>
<body>
<form id="form" name="form" action="/clip/pointmain/main.do">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	<input type="hidden"  id="debug" name="debug" value="${debug}"/>
	
	<input type="hidden"  id="pageType" name="pageType" value="" />
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	
	
	
	<div class="header">
		<h1 class="page_title">포인트 전환</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="javascript:fn.btnAct.redirect('2')">
				<!-- 170719 수정 -->
				<span class="title">클립포인트</span>
				<!-- // 170719 수정 -->
				<strong class="point usepointValue"><span id="mypoint">0</span><span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section">
			<div class="section_inner">
				<!-- 포인트 전환 -->
				<div class="point_transform_wrap">
					<p class="top_title">
						<strong class="point"><span id="sumPoint">0</span><span>P</span></strong>가 성공적으로<br>
						전환 되었습니다.
					</p>
					<!-- [D]2017.07.14 수정-->
					<ul class="point_transform_list">
						<c:if test="${pointInfo.shinhanPoint.result ne 'F' and  pointInfo.shinhanPoint.result ne 'N' }" >
							<li>
								<span class="card_name card_shihan"><img src="../images/temp/card_shinhan.png" alt="ShinhanCard"></span>
								<c:if test="${pointInfo.shinhanPoint.result ne 'N'}"><strong id="shinhanPoint"><fmt:formatNumber value="${pointInfo.shinhanPoint.point}" pattern="#,###" /><i>P</i></strong></c:if>
							</li>
						</c:if>
						<c:if test="${pointInfo.hanaPoint.result ne 'F' and pointInfo.hanaPoint.result ne 'C' and pointInfo.hanaPoint.result ne 'N'}" >
							<li>
								<span class="card_name card_hana"><img src="../images/temp/card_hana.png" alt="하나카드"></span>
								<strong id="hanaPoint"><fmt:formatNumber value="${pointInfo.hanaPoint.point}" pattern="#,###" /><i>P</i></strong>
							</li>
						</c:if>
						<c:if test="${pointInfo.bcPoint.result ne 'F' and pointInfo.bcPoint.result ne 'C' and pointInfo.bcPoint.result ne 'N'}" >
						<li>
							<span class="card_name card_bc"><img src="../images/temp/card_bccard.png" alt="BC카드"></span>
							<strong id="bcPoint"><fmt:formatNumber value="${pointInfo.bcPoint.point}" pattern="#,###" /><i>P</i></strong>
						</li>
						</c:if>
						<c:if test="${pointInfo.kbPoint.result ne 'F' and pointInfo.kbPoint.result ne 'C' and pointInfo.kbPoint.result ne 'N'}" >
						<li>
							<span class="card_name card_kb"><img src="../images/temp/card_kb.png" alt="KB국민카드"></span>
							<strong id="kbPoint"><fmt:formatNumber value="${pointInfo.kbPoint.point}" pattern="#,###" /><i>P</i></strong>
						</li>
						</c:if>
					</ul>
					<a href="javascript:;" onclick="javascript:fn.btnAct.redirect('0')" class="btn_point_more">포인트 더 모으러 가기!</a>
					<a href="javascript:;" onclick="javascript:fn.btnAct.redirect('1')"  class="btn_point_use">포인트 사용하러 가기!</a>
					<!-- [D]2017.07.14 수정-->
					
<%-- 					<c:if test="${pointInfo.shinhanPoint.result eq 'F' || pointInfo.hanaPoint.result eq 'F'}" > <!--  || pointInfo.kbPoint.result eq 'F'} -->
						<p class="point_info">
							포인트 전환 실패의 경우 보유포인트가 부족하거나 서버 통신이 원활하지 않을 수 있습니다. 잠시 후 다시 시도해 주세요
						</p>
					</c:if> --%>
					
					<%-- <c:if test="${pointInfo.hanaPoint.result eq 'F'"} >
						<p class="point_info">
							포인트 전환 한도 초과의 경우 카드사의 전환 가능한 한도를 확인 후 다시 시도해주세요.
						</p>
					</c:if> --%>
					
				</div>
				<!-- // 포인트 전환 -->
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/pointCommon.js"></script>

<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>

<script type="text/javascript">
$(function() {
	 
	 common.invoker.invoke("myPoint"); 
	
	$(document).ready(function(){
		
		var shinhanPoint = <c:out value="${pointInfo.shinhanPoint.point}" />
		var hanaPoint = <c:out value="${pointInfo.hanaPoint.point}" />
		var kbPoint = <c:out value="${pointInfo.kbPoint.point}" />
		var bcPoint = <c:out value="${pointInfo.bcPoint.point}" />
		
		if(	isNaN(shinhanPoint) ) {
			shinhanPoint =0;
		}
		
		if(	isNaN(hanaPoint) ) {
			hanaPoint =0;
		}
		
		if(	isNaN(bcPoint) ) {
			bcPoint =0;
		}

		if(	isNaN(kbPoint) ) {
			kbPoint =0;
		}
		
		var sumPoint = Number(shinhanPoint)+Number(hanaPoint)+Number(bcPoint)+Number(kbPoint);
		
		$("#sumPoint").text(comma(sumPoint));
		
	});
	
	$('.point').click(function() {
		movePage("/clip/pointmain/main.do",'form');
	});
	
	
});

var fn = {
		btnAct : {
			redirect : function(pageType) {
				$('#pageType').val(pageType);
				$('#form').submit();
			}
		}
}

</script>
</body>
</html>