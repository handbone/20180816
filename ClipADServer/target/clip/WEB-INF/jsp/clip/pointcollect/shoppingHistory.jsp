<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- as-is -->
<!-- 
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" href="../css/jquery.bxslider.css"/>
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>
-->



<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body class="bg_on">
<form id="form" name="form" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="uId" name="uId" value="${nextSaving.uId}" />
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">쇼핑 적립 이용 내역</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<ul class="earn_tab tab_chg">
			<li class="active">익월 적립</li>
			<li>익익월 적립</li>
		</ul>
		<!-- 익월적립 -->
		<div class="earn_tab_area tab_con">
			<!-- 
			<c:if test="${history.reportItemList ne null}">
				<ul class="earn_list">
					<c:forEach var="merchant" items="${history.reportItemList}">
						<li>
							<div class="price">
								<span><c:out value="${merchant.merchant_name}" /></span>
								<em>
									<strong><fmt:parseNumber var="sales" value="${merchant.sales}" pattern="0,000"/><fmt:formatNumber value="${sales}" pattern="0,000"/></strong>원
								</em>
							</div>
							<div class="point">
								<span>
									<fmt:parseDate var="merchantDate" value="${merchant.yyyymmdd}" pattern="yyyyMMdd" />
									<fmt:formatDate value="${merchantDate}" pattern="yyyy.MM.dd" />
								</span>
								<em class="plus">
									+<fmt:parseNumber var="hhmiss" value="${merchant.hhmiss}" pattern="0,000"/><fmt:formatNumber value="${hhmiss}" pattern="0,000"/><i>P</i>
								</em>
							</div>
						</li>
					</c:forEach>
				</ul>
			</c:if>
			<c:if test="${history.reportItemList eq null}">
				<div class="earn_nolist">
					최근 쇼핑 익월 적립 이용 내역이 없습니다.
				</div>
			</c:if> 
			-->
		</div>
		<!-- 익익월적립 -->
		<!-- 
		<div class="earn_tab_area tab_con">
			<c:if test="${history.reportItemList ne null}">
				<ul class="earn_list">
					<c:forEach var="merchant" items="${history.reportItemList}">
						<li>
							<div class="price">
								<span><c:out value="${merchant.merchantName}" /></span>
								<em>
									<strong><fmt:parseNumber var="sales" value="${merchant.sales}" pattern="0,000"/><fmt:formatNumber value="${sales}" pattern="0,000"/></strong>원
								</em>
							</div>
							<div class="point">
								<span>
									<fmt:parseDate var="merchantDate" value="${merchant.yyyymmdd}" pattern="yyyyMMdd" />
									<fmt:formatDate value="${merchantDate}" pattern="yyyy.MM.dd" />
								</span>
								<em class="plus">
									+<fmt:parseNumber var="hhmiss" value="${merchant.hhmiss}" pattern="0,000"/><fmt:formatNumber value="${hhmiss}" pattern="0,000"/><i>P</i>
								</em>
							</div>
						</li>
					</c:forEach>
				</ul>
			</c:if>
			<c:if test="${history.reportItemList eq null}">
				<div class="earn_nolist">
					최근 쇼핑 익익월 적립 이용 내역이 없습니다.
				</div>
			</c:if>
		</div>
		 -->
	</div><!-- // contents -->
	<!-- 170728 추가 -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!-- //170728 추가 -->
	<!--  180119 popup 추가-->
	<%@include file="/WEB-INF/jsp/clip/alert_popup.jsp"%>
	<!--  //180119 popup 추가 -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script>
//180119 jyi 추가
function popup_confirm_Act(){
	var type = $('#popup_type').val();
	if(type == ''){
		$('#alert_popup').hide();
	}else{
		$('#alert_popup').hide();
	}
	
}
// //180119 jyi 추가
	$(function(){
		fn.tabChg.init();
		$(window).scroll(fn.list.infiniteScroll);
	})
	
	var fn = {		
		// 페이징 관련 변수	
		bottomOffset : 100,
		listLength : 20,
		currPage : 1,
		totalPage : 0,
		totalCnt : 0,
		ajax_loading : false,
		loadeAll : false,
		
		initBuyHistoryList : null,
		preBuyHistoryEnd : 0,
		buyHistoryListLength : 0,
		buyHistoryList : null,
		buyHistoryStartCnt : 0,
		buyHistoryEndCnt : 0,
		
		inserthtml : null,
		
		tabChg : {
		    init : function(){
		        this.$tab_tg1 = jQuery('.tab_chg li');
		        this.tabEvent();
		        fn.list.connect($('.active'), 1);
		    },
		    tabEvent : function() {
		        this.$tab_tg1.click(function(e) {
		        	var $this = jQuery(this);
		            var $index = $this.index();
		            fn.list.connect($this, ($index+1));
		        });
		    },
		    tabHide : function() {
		        this.$tab_tg1.removeClass('active');
		    }
		},
		list : {
			// 구매적립내역 조회
			connect : function(obj, idx) {
				// 비동기통신 있을 때 마다 초기화
		    	fn.preBuyHistoryEnd = 0;
		    	
		    	$.ajax({
					url : '<c:url value="/pointcollect/shoppingHistoryAjax.do" />',
					type : 'post',
					dataType : 'Json',
					data : {
						list_gb : idx,
						user_token : $("#user_token").val()
					}, 
					beforeSend : function() {
						loadingClip.openClip();
					},
					success : function(resData) {
						$(window).scrollTop(0);
						
						console.log(resData);
						
						fn.buyHistoryList = resData.history.order_list;
						fn.totalCnt = fn.buyHistoryList.length
						
						// 비동기 통신 시 리스트 초기화(삭제)
						$('.earn_tab_area').html('');
						// 페이징에 사용하기 위해 넣어줌
						// fn.initBuyHistoryList = fn.buyHistoryList;
						fn.buyHistoryListLength = fn.buyHistoryList.length;						
						// 페이징 및 스크롤 초기화 작업
						fn.totalCnt = fn.buyHistoryListLength;
						if(fn.totalCnt%fn.listLength == 0) {
							fn.totalPage = fn.totalCnt/fn.listLength;
						} else {
							fn.totalPage = parseInt(fn.totalCnt/fn.listLength)+1;
						}
						fn.loadedAll = false;
						fn.currPage = 1;
						
						$('.earn_tab_area').html('<ul class="earn_list"></ul>');
						fn.list.drawBuyHistoryList(obj);
					},
					error : function(errData) {
						/* alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.'); */
						// 180119 jyi 추가
				    		var _text = '네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.';
						$('#notice_word').empty();
						$('#notice_word').append(_text);
						$('#popup_type').val('default');
						$('#alert_type').hide();
						$('#alert_popup').show();
						// //180119 jyi 추가
						console.log(errData);
					},
					complete : function() {
						loadingClip.closeClip();
					}
				});
		    },
		    // 구매적립내역 출력
		    drawBuyHistoryList : function(obj) {
				fn.buyHistoryStartCnt = 0;
				
				// 출력할 리스트 시작과 끝 지점 초기화
				if(fn.preBuyHistoryEnd == 0) fn.buyHistoryStartCnt = 0;
	    		else fn.buyHistoryStartCnt = fn.preBuyHistoryEnd;
				fn.buyHistoryEndCnt = fn.buyHistoryStartCnt + fn.listLength;
				
				fn.inserthtml = "";
		    	if(fn.buyHistoryListLength > 0) {
					// $.each(buyList, function(index, item) {
					for(var i=fn.buyHistoryStartCnt; i<fn.buyHistoryEndCnt; i++) {
						
						if(i > (fn.buyHistoryListLength-1)) break;
						
						fn.inserthtml += '<li>';
						fn.inserthtml += '<div class="price">';
						fn.inserthtml += '<span>' + fn.buyHistoryList[i].merchant_name + '</span>'
						fn.inserthtml += '<em><strong>' + util.numberFormat(fn.buyHistoryList[i].sales) + '</strong>원</em>';
						fn.inserthtml += '</div>';
						fn.inserthtml += '<div class="point">';
						fn.inserthtml += '<span>' + util.dateFormat(fn.buyHistoryList[i].yyyymmdd) + '</span>';
						fn.inserthtml += '<em class="plus">+' + util.numberFormat(fn.buyHistoryList[i].commission) + '<i>P</i></em>';
						fn.inserthtml += '</div>';
						fn.inserthtml += '</span>';
						fn.inserthtml += "</li>";
					}
					// });
					$('.earn_list').append(fn.inserthtml);
					
		            if(obj != null) {
						fn.tabChg.tabHide();
		            	obj.addClass('active');
		            }		            
		            fn.preBuyHistoryEnd = fn.buyHistoryEndCnt;
		            
				} else {
					
					if(obj != null) {
						
						fn.inserthtml += '<div class="earn_nolist">';
						if(obj.index() == 0) {
							fn.inserthtml += '최근 쇼핑 익월 적립 이용 내역이 없습니다.';
						} else {
							fn.inserthtml += '최근 쇼핑 익익월 적립 이용 내역이 없습니다.';
						}
						fn.inserthtml += '</div>';
						
						$('.earn_tab_area').html(fn.inserthtml);
						
						fn.tabChg.tabHide();
			            obj.addClass('active');
			            
					}
				}
		    },
		    // 구매적립내역 인피니트 스크롤
			infiniteScroll : function() {
				if(fn.loadedAll) return;
				// ajax 로딩중이라면 중복 처리 방지를 위해 스킵합니다.
				if(fn.ajax_loading) return;
				if($(window).scrollTop() > $(document).height() - $(window).height() - fn.bottomOffset) {
					if(fn.currPage == fn.totalPage){
						// console.log('더 이상 보여줄 리스트가 없습니다.');
						fn.loadedAll = true;
					} else {
						// console.log('바닥입니다. 리스트 ' + fn.listLength + '개 추가합니다.');
						fn.ajax_loading = true;
						fn.currPage++;
						fn.list.drawBuyHistoryList();
						fn.ajax_loading = false;
					}
				}
			}
		}
	};
	var util = {
		numberFormat : function(x) {
		    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},
		dateFormat : function(x) {
			var year = x.substr(0,4);
			var month = x.substr(4,2);
			var day = x.substr(6,2);
			var date = year+'.'+month+'.'+day;
			return date;
		}
	};
</script>
</body>
</html>