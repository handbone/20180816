package clip.common.util;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class ClipRequesterCode {
	
	private Map<String, String> NAME_MAP;
	
	public String REQ_SWAP_SHINHAN = "pointswap_shinhan";
	public String REQ_SWAP_KB = "pointswap_kb";
	public String REQ_SWAP_HANA = "pointswap_hana";
	public String REQ_SHOP_BUY = "shopbuy";
	public String REQ_PCOUPON = "pcoupon";
	public String REQ_GIFTISHOW = "giftishow";
	public String REQ_MOVIE_CGV = "reservemovie_cgv";
	public String REQ_MOVIE_LOTTE = "reservemovie_lotte";
	public String REQ_POINT_PAY = "pointpay";
	public String REQ_VIP_LOUNGE = "viplounge";
	public String REQ_BZA = "BZA";
	public String REQ_BZS = "BZS";
	public String REQ_POINT_BOMB = "pointbomb";
	
	@PostConstruct
	private void init(){
		NAME_MAP = new HashMap<String, String>();

		NAME_MAP.put(REQ_SWAP_SHINHAN, "마이신한포인트");
		NAME_MAP.put(REQ_SWAP_KB, "KB카드");
		NAME_MAP.put(REQ_SWAP_HANA, "하나머니");
		NAME_MAP.put(REQ_SHOP_BUY, "쇼핑적립");
		NAME_MAP.put(REQ_PCOUPON, "클립포인트 쿠폰 등록");
		NAME_MAP.put(REQ_GIFTISHOW, "기프티쇼");
		NAME_MAP.put(REQ_MOVIE_CGV, "CGV 영화예매");
		NAME_MAP.put(REQ_MOVIE_LOTTE, "롯데");
		NAME_MAP.put(REQ_POINT_PAY, "매장바로쓰기");
		NAME_MAP.put(REQ_VIP_LOUNGE, "VIP 라운지");
		NAME_MAP.put(REQ_BZA, "즉시적립");
		NAME_MAP.put(REQ_BZS, "잠금화면");
		NAME_MAP.put(REQ_POINT_BOMB, "포인트폭탄");
		
		
		
	}
	
	public String getName(String requester_code){
		if(NAME_MAP.containsKey(requester_code)) {
			return NAME_MAP.get(requester_code);
		}
		
		return "이벤트";
	}
	
}
