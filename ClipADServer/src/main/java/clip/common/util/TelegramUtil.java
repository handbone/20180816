package clip.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.codehaus.plexus.util.StringUtils;

public class TelegramUtil
{
	private static int SERIAL_NO;
	
	public synchronized static String createSimpleTrId(){
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		SimpleDateFormat df = new SimpleDateFormat("yyMMddhhmmss");
		
		int temp = ++SERIAL_NO;
		if(temp == 10000)
			SERIAL_NO = 0;
		
		return df.format(currentTime) + StringUtils.leftPad(""+(temp), 4, "0");
	}
	
	/**
	 * 클립 포인트 서버와 통신시에 사용하는 올드 타입 TR_ID 생성 
	 * @param serviceIdx
	 * @return
	 * @throws Exception
	 */
	public static String makeTrIdForClip(String serviceIdx) throws Exception {
		// tr_id(사용자의 유일한 값) 설정
		String tr_id = String.valueOf(System.currentTimeMillis());
		if("".equals(tr_id) || null == tr_id){
			throw new java.lang.Exception();
		}
		StringBuffer sb2 = new StringBuffer(tr_id);
		
		if (tr_id.length() < 16) {
			sb2.append(serviceIdx.substring(serviceIdx.length()-3, serviceIdx.length()));
		}
		if (tr_id.length() < 16) {
			while (sb2.length() < 16) {
				sb2.append("0");
			}
		}
		return sb2.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(""+System.currentTimeMillis());
		System.out.println(createSimpleTrId());
		System.out.println(createSimpleTrId());
		System.out.println(createSimpleTrId());
		System.out.println(createSimpleTrId());
		System.out.println(createSimpleTrId());
		System.out.println(""+System.currentTimeMillis());
	}
}