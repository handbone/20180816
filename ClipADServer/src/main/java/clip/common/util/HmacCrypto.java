package clip.common.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import clip.common.crypto.AES256CipherClip;

public class HmacCrypto {
	
	public static final String HMAC_ALGO = "HmacSHA256";
	public static final String HMAC_KEY = "9oCloluq1enq4Dc1pVLJOyBbhnDZyfRV";
	
	public static String getHmacVal(String val) {
		return getHmacVal(HMAC_KEY, val);
	}

	public static String getHmacVal(String key, String val) {
		try {
			Mac sha256_HMAC = Mac.getInstance(HMAC_ALGO);
			SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), HMAC_ALGO);
			sha256_HMAC.init(secret_key);

			return bytesToHex(sha256_HMAC.doFinal(val.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes) result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }
	
	public static void main(String[] args){
		String key = AES256CipherClip.AES_Decode("ci+5JU6bvKxpXJEsvpQ5zg==") + "GRTRM00002" + "0000000000000000000000000000000";
		
		 System.out.println(getHmacVal(key, "12345"));
	}
}
