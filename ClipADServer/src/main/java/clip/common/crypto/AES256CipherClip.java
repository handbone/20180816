/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/


package clip.common.crypto;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.coopnc.cardpoint.util.MessageUtil;

public class AES256CipherClip {

	static Logger logger = Logger.getLogger(AES256CipherClip.class.getName());
	
	final static String TAG = "AES256Cipher";
	private static byte[] ivBytes = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	private static String key = "ZWCloluqfenq4Dc1pVLJOFBbmnDZyfRV";
	
	public static String AES_Encode(String textStr) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		SecretKeySpec newKey = new SecretKeySpec(key.getBytes(), "AES");
		
		//logger.debug("newKey::"+newKey);
		
		/* java는 기본적으로 AES128까지밖에 지원을 안하기때문에 AES256을 사용하려면 
		 AES256을 사용할 수 있도록 하는 jar파일을 교체해주어야 한다.
		
		 http://www.oracle.com/technetwork/java/javase/downloads/index.html 에서
		 Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files 8 Download 에 해당하는
		local_policy.jar , US_export_policy.jar 을 다운받는다.
		
		그 두개의 파일을 $JAVA_HOME/jre/lib/security ( C:\Program Files\Java\jdk1.8.0_91\jre\lib\security ) 에 복사한다.
		
		현재 프로젝트에서는 해당 파일을 resources 하위 디렉토리에 복사해두었다.
		 */
		
		Cipher cipher = null;
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
		
		return new String(Base64.encodeBase64(cipher.doFinal(textStr.getBytes("UTF-8"))));
		
	}

	public static String AES_Decode(String temptextStr) {
		
		String decStr = "";
		String textStr = temptextStr;
		
		try {
		
			byte[] textBytes = Base64.decodeBase64(textStr.replaceAll(" ", "+"));
			
			AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			SecretKeySpec newKey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
			
			decStr = new String(cipher.doFinal(textBytes));
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			decStr = "";
		}
		
		return decStr;
		
	}
	
	public static void main(String[] args) {
		try {
			
			//String sample = "test";
	        //String encodeData = AES256CipherClip.AES_Encode(sample);
	        //String decodeData = new String(AES256CipherClip.AES_Decode(encodeData));
	        //decodeData = new String(AES256CipherClip.AES_Decode("Hq5XJ9RrqrPu+vtvDg9Azg=="));
	        
			//System.out.println(MessageUtil.getDateStr("MMddHHmmss"));
			
			//String decodeData = new String(AES256CipherClip.AES_Decode("nkin4TO9AugLgDf9knAMjItV4xwxrhb5ZhoLjhpXEknH6gMUimXbWZLp84wNpVGUa2Y1R4kaAD2kDwdvevW97JqPMhVPjCQEMbva"));
			
	        //System.out.println(encodeData);
	        //System.out.println(decodeData);
			
//			String date = MessageUtil.getDateStr("yyyyMMddHHmmss");
//			System.out.println(date.substring(4, 8));
//			String s="267";
//			int i=Integer.parseInt(s);
//			
//					
//			String ss=Integer.toBinaryString(i);
//			
//			System.out.println(ss.substring(0,1));
//			System.out.println(ss.substring(1,9));
//			byte a=Byte.parseByte(ss.substring(0, 1), 2);
//			byte b=Byte.parseByte(ss.substring(1, 9), 2);
//			System.out.println(a);
//			System.out.println(b);
			
//			String len ="267";
//			short aaa = Short.parseShort(len);
//			byte[] bs = new byte[2];
//			bs[0]=(byte)((aaa>>8) & 0xff);
//			bs[1]=(byte)(aaa & 0xff);
//			
//			System.out.println(bs[0]);
//			System.out.println(bs[1]);
			
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
}
