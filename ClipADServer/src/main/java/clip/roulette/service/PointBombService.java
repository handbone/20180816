/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import clip.roulette.bean.BuzzAdItem;
import clip.roulette.bean.PointBombInfo;

public interface PointBombService {

	public PointBombInfo updatePointBombInfoByLockScreenNoti(PointBombInfo pointBombInfo) throws Exception;
	
	public PointBombInfo levelUp(PointBombInfo pointBombInfo) throws Exception;
	
	public PointBombInfo getPointBombInfo(PointBombInfo pointBombInfo) throws Exception;
	
	public List<BuzzAdItem> getOfferwallAdList() throws Exception;
	
	public PointBombInfo checkPointBombAuth(PointBombInfo pointBombInfo) throws Exception;
	
	public PointBombInfo checkOfferwallAdAuth(PointBombInfo pointBombInfo, HttpServletRequest request) throws Exception;
	
	public PointBombInfo getPointRouletteJoin(PointBombInfo param) throws java.lang.Exception;
	
	public PointBombInfo rouletteComplete(PointBombInfo pointBombInfo, HttpServletRequest request) throws Exception;
	
}
