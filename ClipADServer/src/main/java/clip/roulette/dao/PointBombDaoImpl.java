/*[CLiP Point] version [v3.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import clip.roulette.bean.BuzzAdItem;
import clip.roulette.bean.PointBombHist;
import clip.roulette.bean.PointBombInfo;

@Repository
public class PointBombDaoImpl implements PointBombDao {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;

	@Override
	public PointBombInfo selectPointBombInfo(PointBombInfo pointBombInfo) throws Exception {
		return sqlSession.selectOne("PointBomb.selectPointBombInfo", pointBombInfo);
	}

	@Override
	public int insertPointBombInfo(PointBombInfo pointBombInfo) throws Exception {
		sqlSession.insert("PointBomb.insertPointBombInfo", pointBombInfo);
		return 1;
	}

	@Override
	public int updatePointBombInfo(PointBombInfo pointBombInfo) throws Exception {
		sqlSession.update("PointBomb.updatePointBombInfo", pointBombInfo);
		return 1;
	}
	
	@Override
	public int updatePointBombInfoForReward(PointBombInfo pointBombInfo) throws Exception {
		sqlSession.update("PointBomb.updatePointBombInfoForReward", pointBombInfo);
		return 1;
	}

	@Override
	public PointBombInfo selectPointBombDetailInfo(PointBombInfo pointBombInfo) throws Exception {
		return sqlSession.selectOne("PointBomb.selectPointBombDetailInfo", pointBombInfo);
	}

	@Override
	public PointBombInfo selectPointBombDayInfo(PointBombInfo pointBombInfo) throws Exception {
		return sqlSession.selectOne("PointBomb.selectPointBombDayInfo", pointBombInfo);
	}

	@Override
	public List<BuzzAdItem> selectBuzzAdItemList() throws Exception {
		return sqlSession.selectList("PointBomb.selectBuzzAdItemList", null);
	}

	@Override
	public int insertPointBombHist(PointBombHist PointBombHist) throws Exception {
		sqlSession.insert("PointBomb.insertPointBombHist", PointBombHist);
		return 1;
	}

	@Override
	public int selectPointBombTRID(@SuppressWarnings("rawtypes") Map map) throws Exception {
		return sqlSession.selectOne("PointBomb.selectPointBombTRID", map);
	}
	
}
