/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.bean;

public class RouletteDto {

	private String roulette_id;
	private String keyid;
	private String keytype;
	private String user_ci;
	private String cust_id;
	private String ga_id;
	private String ctn;
	private String ipaddr;
	private String status;
	private String check01;
	private String check02;
	private String returnPage;
	private String returnURL;
	
	
	public String getRoulette_id() {
		return roulette_id;
	}

	public void setRoulette_id(String roulette_id) {
		this.roulette_id = roulette_id;
	}

	public String getKeyid() {
		return keyid;
	}

	public void setKeyid(String keyid) {
		this.keyid = keyid;
	}

	public String getKeytype() {
		return keytype;
	}

	public void setKeytype(String keytype) {
		this.keytype = keytype;
	}

	public String getUser_ci() {
		return user_ci;
	}

	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getGa_id() {
		return ga_id;
	}

	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}

	public String getCtn() {
		return ctn;
	}

	public void setCtn(String ctn) {
		this.ctn = ctn;
	}

	public String getIpaddr() {
		return ipaddr;
	}

	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCheck01() {
		return check01;
	}

	public void setCheck01(String check01) {
		this.check01 = check01;
	}

	public String getCheck02() {
		return check02;
	}

	public void setCheck02(String check02) {
		this.check02 = check02;
	}

	public String getReturnPage() {
		return returnPage;
	}

	public void setReturnPage(String returnPage) {
		this.returnPage = returnPage;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

}
