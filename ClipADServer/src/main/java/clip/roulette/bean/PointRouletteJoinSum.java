/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.bean;

public class PointRouletteJoinSum {
	
	private String max_plus;
	
	private String idx;
	 private String roulette_id;
	 private String sdate;
	 private String area01;
	 private String area02;
	 private String area03;
	 private String area04;
	 private String area05;
	 private String area06;
	 
	 
	public String getMax_plus() {
		return max_plus;
	}
	public void setMax_plus(String max_plus) {
		this.max_plus = max_plus;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getRoulette_id() {
		return roulette_id;
	}
	public void setRoulette_id(String roulette_id) {
		this.roulette_id = roulette_id;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public String getArea01() {
		return area01;
	}
	public void setArea01(String area01) {
		this.area01 = area01;
	}
	public String getArea02() {
		return area02;
	}
	public void setArea02(String area02) {
		this.area02 = area02;
	}
	public String getArea03() {
		return area03;
	}
	public void setArea03(String area03) {
		this.area03 = area03;
	}
	public String getArea04() {
		return area04;
	}
	public void setArea04(String area04) {
		this.area04 = area04;
	}
	public String getArea05() {
		return area05;
	}
	public void setArea05(String area05) {
		this.area05 = area05;
	}
	public String getArea06() {
		return area06;
	}
	public void setArea06(String area06) {
		this.area06 = area06;
	}
	 
	 
}
