/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import clip.roulette.bean.PointRoulette;
import clip.roulette.bean.PointRouletteJoin;
import clip.roulette.bean.RouletteDto;
import clip.roulette.service.RouletteService;

@Controller
public class NewRouletteController {
	Logger logger = Logger.getLogger(NewRouletteController.class.getName());

	@Autowired
	private RouletteService rouletteService;
	
	/*
	 * 룰렛 메인페이지
	 */
	@RequestMapping(value = "/new_roulette/rouletteMain.do")
	public ModelAndView rouletteMain(
			@RequestParam(required = true) String roulette_id,
			@RequestParam(required = true) String keyid, 
			@RequestParam(required = true) String keytype,
			@RequestParam(required = false) String returnURL, 
			RouletteDto temprouletteDto,
			PointRouletteJoin temppointRouletteJoin, 
			HttpServletRequest request
			) throws java.lang.Exception {
		logger.debug("rouletteMain Page start!!!");
		
		logger.debug("Parameter ====================");
		logger.debug("roulette_id : "+roulette_id);
		logger.debug("keyid : "+keyid);
		logger.debug("keytype : "+keytype);
		logger.debug("returnURL : "+returnURL);
		logger.debug("ga_id : " + temppointRouletteJoin.getGa_id());

		String ga_id = temppointRouletteJoin.getGa_id();
		
		if("".equals(keyid) || null == keyid){
			logger.debug("CLIPPOINTERROR 100:'keyid' parameter nothing");
			throw new java.lang.Exception();
		}
		if("".equals(roulette_id) || null == roulette_id){
			logger.debug("CLIPPOINTERROR 101:'roulette_id' parameter nothing");
			throw new java.lang.Exception();
		}
		
		
		RouletteDto rouletteDto = temprouletteDto;
		PointRouletteJoin pointRouletteJoin = temppointRouletteJoin;
		
		// id선택 및 입력
		rouletteDto = rouletteService.selectId(rouletteDto);
		
		ModelAndView mav = new ModelAndView();

		// roulette_id에 따라 이벤트가 진행중이면 false -> 이벤트 종료되었습니다.
		PointRoulette pointRoulette = rouletteService.selectPointRoulette(roulette_id, request);
		
		if ("null".equals(String.valueOf(pointRoulette))) {
			// 이벤트가 기간이 지났어도 이미지는 띄워줘야 해서 정보는 가져온다.
			pointRoulette = rouletteService.pointRouletteNoChkDate(roulette_id, request);
			mav.addObject("pointRoulette", pointRoulette );
			mav.addObject("msg", "EndEvent");
			mav.setViewName("/clip/new_roulette/rouletteMain");
			return mav;
		}
		
		// Keytype에 따른 화면 분기
		
		if("1".equals(rouletteDto.getKeytype())){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
		}else if("3".equals(rouletteDto.getKeytype()) ){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			int agreeChk = rouletteService.agreeChk(rouletteDto);
			if(agreeChk == 0){
				logger.debug("ctn:"+rouletteDto.getCtn());
				return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=3&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain");	
			}
		}else if("2".equals(rouletteDto.getKeytype()) ){
			logger.debug("View Distribute By Keytype : "+rouletteDto.getKeytype());
			int agreeChk = rouletteService.agreeChk(rouletteDto);
			if(agreeChk == 0){
				logger.debug("ga_id:"+rouletteDto.getGa_id());
				return new ModelAndView("redirect:/roulette/agreeForm.do?roulette_id="+rouletteDto.getRoulette_id()+"&keytype=2&keyid="+rouletteDto.getKeyid()+"&returnPage=rouletteMain"+"&returnURL="+returnURL);	
			}
		}
		
		
		pointRouletteJoin = rouletteService.selectId(rouletteDto, pointRouletteJoin);

		mav.addObject("pointRoulette", pointRoulette);
		mav.addObject("rouletteDto", rouletteDto);
		
		// 1인당 1일 1회인지 체크 -> 오늘은 참여하셨네요. 내일 또 참여바랍니다.
		int dailyCnt = rouletteService.getDailyCntChk(rouletteDto);
		logger.debug("Daily Count By Id : "+dailyCnt+" Count");
		if ( 0 < dailyCnt ) {
			mav.addObject("msg", "complete");
			mav.addObject("resultPointRouletteJoin", pointRouletteJoin);
			mav.setViewName("/clip/new_roulette/rouletteMain");
			return mav;
		}else{
			
			// 당첨값 셋팅
			PointRouletteJoin resultPointRouletteJoin = rouletteService.getPointRouletteJoin(pointRoulette, rouletteDto,
					pointRouletteJoin, request);
			
			resultPointRouletteJoin.setGa_id(ga_id);
			
			mav.addObject("resultPointRouletteJoin", resultPointRouletteJoin);
			mav.addObject("msg", "");
			mav.setViewName("/clip/new_roulette/rouletteMain");
			return mav;
			
		}
		
		
	}
	
	
	/*
	 * 룰렛 메인페이지에서 룰렛을 돌리는 순간 호출되는 클래스이다
	 * ( 룰렛을 돌리는 순간 모든 이벤트가 끝나며 화면에서는 룰렛이 멈춘 후 결과값을 보여준다 ) 
	 */
	@RequestMapping(value = "/new_roulette/rouletteComplete.do", method = { RequestMethod.POST })
	@ResponseBody
	public String rouletteComplete(RouletteDto temprouletteDto, PointRouletteJoin pointRouletteJoin,
			HttpServletRequest request, HttpServletResponse response

	) throws java.lang.Exception {
		logger.debug("rouletteComplete Page start!!!");
		
		RouletteDto rouletteDto = temprouletteDto;
		
		Map<String,String> map = new HashMap<String,String>();
		
		// 1인당 1일 1회인지 체크
		int dailyCnt = rouletteService.getDailyCntChk(rouletteDto);
		if ( 0 < dailyCnt ) {
			map.put("result", "complete");
			return JSONValue.toJSONString(map);
		}else{
			// 포인트 api 연동 + 연동로그 입력 + 당첨값 확정 update + 합계테이블 업데이트(있는지 확인)
			
			int result =  0;
			
			try{
			
			 result = rouletteService.rouletteComplete(rouletteDto, pointRouletteJoin, request);
			
			}catch(Exception e){
				e.printStackTrace();
			}
			if (result == 1) {
				map.put("result", "success");
				return JSONValue.toJSONString(map);
			}else if (result == 2) {
				map.put("result", "max06Change");
				return JSONValue.toJSONString(map);
			}else if (result == 3) {
				map.put("result", "complete");
				return JSONValue.toJSONString(map);
			}else {
				map.put("result", "false");
				return JSONValue.toJSONString(map);
			}
		}
		

	}

}
