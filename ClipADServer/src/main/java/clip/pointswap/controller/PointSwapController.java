package clip.pointswap.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.coopnc.cardpoint.consts.Consts.BcConsts;
import com.coopnc.cardpoint.consts.Consts.HanaConsts;
import com.coopnc.cardpoint.consts.Consts.KbConsts;
import com.coopnc.cardpoint.consts.Consts.ShinhanConsts;
import com.coopnc.cardpoint.util.MessageUtil;

import clip.integration.bean.BcCardMessage;
import clip.integration.bean.HanaCardMessage;
import clip.integration.bean.KbCardMessage;
import clip.integration.bean.ShinhanCardMessage;
import clip.integration.service.BcCardClientService;
import clip.integration.service.HanaCardClientService;
import clip.integration.service.KbCardClientService;
import clip.integration.service.ShinhanCardClientService;

@Controller
public class PointSwapController {
	Logger logger = Logger.getLogger(PointSwapController.class.getName());
	
	@Autowired
	private HanaCardClientService hanaCardClientService;

	@Autowired
	private ShinhanCardClientService shinhanCardClientService;
	
	@Autowired
	private BcCardClientService bcCardClientService;
	
	@Autowired
	private KbCardClientService kbCardClientService;
	
	@RequestMapping(value = "/pointSwap/cancelMinusPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String cancelMinusPoint(@RequestParam(required = true) String regSource,
			@RequestParam(required = true) String userCi, @RequestParam(required = true) String transactionId,
			@RequestParam(required = true) int point, @RequestParam(required = false) String custIdOrg,
			@RequestParam(required = false) String approveNo,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		HashMap<String, String> map = new HashMap<>();
		String user_ci = URLDecoder.decode(userCi, "utf-8");
		String trans_id = URLDecoder.decode(transactionId, "utf-8");
		String cust_id_org = URLDecoder.decode(custIdOrg, "utf-8");
		String approve_no = URLDecoder.decode(approveNo, "utf-8");
		
		logger.debug("cancel point start!!! parameter regSource[" + regSource + "],userCi[" + user_ci + "],transactionId[" + trans_id + "],custIdOrg[" + cust_id_org + "],approveNo[" + approve_no + "]");
		logger.debug("cancelMinusPoint WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		//----------------- 전송시 사용할 각각의 메시지 작성하여 통신 ---------------------
		if (HanaConsts.system_name.equals(regSource)) {
			//하나멤버스 메시지
			HanaCardMessage hanaMsg = new HanaCardMessage();
			hanaMsg.setUser_ci(user_ci);
			hanaMsg.setTrans_id(trans_id);
			hanaMsg.setReq_point(point);
			Future<HanaCardMessage> hanaResult = hanaCardClientService.cancelMinusPoint(hanaMsg);
			HanaCardMessage hanaPoint = hanaResult.get();
			
			logger.info("cancelMinusPoint Response hana result["+hanaPoint.getResult()+"],resultCode["+hanaPoint.getRes_code()+"],resultMsg[" + hanaPoint.getRes_message() + "]");
			
			map.put("result", hanaPoint.getResult());
			map.put("resultMsg", hanaPoint.getRes_code());
		} else if (ShinhanConsts.system_name.equals(regSource)) {
			ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
			shinhanMsg.setUser_ci(user_ci);
			shinhanMsg.setTrans_id(trans_id);
			shinhanMsg.setReq_point(point);
			shinhanMsg.setCust_id_org(cust_id_org);
			Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.cancelMinusPoint(shinhanMsg);
			ShinhanCardMessage shinhanPoint = shinhanResult.get();

			logger.info("cancelMinusPoint Response shinhan result["+shinhanPoint.getResult()+"],resultCode["+shinhanPoint.getRes_code()+"],resultMsg[" + shinhanPoint.getRes_message() + "]");
			
			map.put("result", shinhanPoint.getResult());
			map.put("resultMsg", shinhanPoint.getRes_code());
		} else if (BcConsts.system_name.equals(regSource)) {
			BcCardMessage bcMsg = new BcCardMessage();
			bcMsg.setUser_ci(user_ci);
			bcMsg.setTrans_id(trans_id);
			bcMsg.setReq_point(point);
			bcMsg.setApprove_no(approve_no);
			Future<BcCardMessage> bcResult = bcCardClientService.cancelMinusPoint(bcMsg);
			BcCardMessage bcPoint = bcResult.get();

			logger.info("cancelMinusPoint Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"],resultMsg[" + bcPoint.getRes_message() + "]");
			
			map.put("result", bcPoint.getResult());
			map.put("resultMsg", bcPoint.getRes_code());
		} else if (KbConsts.system_name.equals(regSource)) {
			KbCardMessage kbMsg = new KbCardMessage();
			kbMsg.setUser_ci(user_ci);
			kbMsg.setTrans_id(trans_id);
			kbMsg.setReq_point(point);
			Future<KbCardMessage> kbResult = kbCardClientService.cancelMinusPoint(kbMsg);
			KbCardMessage kbPoint = kbResult.get();
			
			logger.info("cancelMinusPoint Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"],resultMsg[" + kbPoint.getRes_message() + "]");
			
			map.put("result", kbPoint.getResult());
			map.put("resultMsg", kbPoint.getRes_code());
		}

		logger.debug("cancelMinusPoint Complete : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		// 포인트 차감
		
		return JSONValue.toJSONString(map);
	}
	
	// BC TEST
	
	@RequestMapping(value = "/pointSwap/getBcPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String getBcPoint(@RequestParam(required = false) String userCi,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		long start = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<>();
		
		logger.info("PointSwap Get Start : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		logger.info("PointSwap Get Parameter : userCi[" + userCi + "]");
		
		BcCardMessage bcMsg = new BcCardMessage();
		
		if (userCi == null || "".equals(userCi)) {
			bcMsg.setUser_ci("C50o9AWJzrFAjo63pcoANnbeZK1zggbyrpTuz5Y1xhkd+hUeAXSbEY7+VLVpQADrUQWN7X1l9mHZzBsTpT3M2w==");		// 현업통해 전달받으면 입력 후 테스트
		} else {
			bcMsg.setUser_ci(userCi);
		}
		
		Future<BcCardMessage> bcResult = bcCardClientService.getPoint(bcMsg);
		
		logger.info("PointSwap Get WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		BcCardMessage bcPoint = bcResult.get();
		
		logger.info("PointSwap Get Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"],point["+bcPoint.getAvail_point()+"]");
		logger.debug("PointSwap Get COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		map.put("result", bcPoint.getResult());
		map.put("resultCode", bcPoint.getRes_code());
		if("S".equals(bcPoint.getResult())) {
			map.put("point", ""+bcPoint.getAvail_point());
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Get card point total Elapsed time : " + (System.currentTimeMillis() - start));
		
		return JSONValue.toJSONString(map);
	}
	
	@RequestMapping(value = "/pointSwap/minusBcPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String minusBcPoint(@RequestParam(required = false) String userCi,
			@RequestParam(required = true) int point,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		logger.debug("minus point start!!! parameter userCi[" + userCi + "]");
		logger.debug("minus point start!!! parameter point[" + point + "]");
		long start = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<>();
		BcCardMessage bcMsg = new BcCardMessage();
		
		if (userCi == null || "".equals(userCi)) {
			bcMsg.setUser_ci("C50o9AWJzrFAjo63pcoANnbeZK1zggbyrpTuz5Y1xhkd+hUeAXSbEY7+VLVpQADrUQWN7X1l9mHZzBsTpT3M2w==");		// 현업통해 전달받으면 입력 후 테스트
		} else {
			bcMsg.setUser_ci(userCi);
		}
		
		if (point <= 0) {
			map.put("result", "F");
			map.put("resultCode", "01");
			
			logger.debug("minus point start!!! point is invalid");
			
			return JSONValue.toJSONString(map);
		}
		
		bcMsg.setReq_point(point);
		Future<BcCardMessage> bcResult = bcCardClientService.minusPoint(bcMsg);
		
		logger.info("PointSwap Use WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		BcCardMessage bcPoint = bcResult.get();
		
		logger.info("PointSwap Use Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"], avail point["+bcPoint.getAvail_point() +"], use point["+bcPoint.getRes_point()+"]");
		logger.debug("PointSwap Use COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		map.put("result", bcPoint.getResult());
		map.put("resultCode", bcPoint.getRes_code());
		if("S".equals(bcPoint.getResult())) {
			map.put("point", ""+bcPoint.getAvail_point());
			map.put("usePoint", bcPoint.getRes_point()+"");
			map.put("approveNo", bcPoint.getApprove_no());
			map.put("transId", bcPoint.getTrans_id());
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Use card point total Elapsed time : " + (System.currentTimeMillis() - start));
		
		return JSONValue.toJSONString(map);
	}
	
	@RequestMapping(value = "/pointSwap/cancelMinusBcPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String cancelMinusBcPoint(@RequestParam(required = false) String userCi,
			@RequestParam(required = true) String transId, @RequestParam(required = true) int point,
			@RequestParam(required = true) String approvalNo,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		logger.debug("cancel minus point start!!! parameter userCi[" + userCi + "]");
		logger.debug("cancel minus point start!!! parameter transId[" + transId + "]");
		logger.debug("cancel minus point start!!! parameter point[" + point + "]");
		logger.debug("cancel minus point start!!! parameter approvalNo[" + approvalNo + "]");
		long start = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<>();
		BcCardMessage bcMsg = new BcCardMessage();

		if (userCi == null || "".equals(userCi)) {
			bcMsg.setUser_ci("C50o9AWJzrFAjo63pcoANnbeZK1zggbyrpTuz5Y1xhkd+hUeAXSbEY7+VLVpQADrUQWN7X1l9mHZzBsTpT3M2w==");		// 현업통해 전달받으면 입력 후 테스트
		} else {
			bcMsg.setUser_ci(userCi);
		}
		
		if (transId == null || "".equals(transId)) {
			map.put("result", "F");
			map.put("resultCode", "02");
			
			logger.debug("cancel minus point start!!! transId is invalid");
			
			return JSONValue.toJSONString(map);
		}
		
		bcMsg.setTrans_id(transId);
		bcMsg.setReq_point(point);
		bcMsg.setApprove_no(approvalNo);	// 접수일련번호가 있어야 취소 가능(필수)
		Future<BcCardMessage> bcResult = bcCardClientService.cancelMinusPoint(bcMsg);
		
		logger.info("PointSwap Cancel WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		BcCardMessage bcPoint = bcResult.get();
		
		logger.info("PointSwap Cancel Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"], avail point["+bcPoint.getAvail_point() +"], use point["+bcPoint.getRes_point()+"]");
		logger.debug("PointSwap Cancel COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		map.put("result", bcPoint.getResult());
		map.put("resultCode", bcPoint.getRes_code());
		if("S".equals(bcPoint.getResult())) {
			map.put("point", ""+bcPoint.getAvail_point());
			map.put("usePoint", bcPoint.getRes_point()+"");
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Cancel card point total Elapsed time : " + (System.currentTimeMillis() - start));
		
		return JSONValue.toJSONString(map);
	}
	
	// KB TEST
	
	@RequestMapping(value = "/pointSwap/getKbPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String getKbPoint(@RequestParam(required = false) String userCi,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		long start = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<>();
		
		logger.info("PointSwap Get Start : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		logger.info("PointSwap Get Parameter : userCi[" + userCi + "]");
		
		KbCardMessage kbMsg = new KbCardMessage();
		
		if (userCi == null || "".equals(userCi)) {
			kbMsg.setUser_ci("TEST000012345678901234567890123456789012345678901234567890123456789012345678901234567890");		// 현업통해 전달받으면 입력 후 테스트
		} else {
			kbMsg.setUser_ci(userCi);
		}
		
		Future<KbCardMessage> kbResult = kbCardClientService.getPoint(kbMsg);
		
		logger.info("PointSwap Get WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		KbCardMessage kbPoint = kbResult.get();
		
		logger.info("PointSwap Get Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"],point["+kbPoint.getAvail_point()+"]");
		logger.debug("PointSwap Get COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		map.put("result", kbPoint.getResult());
		map.put("resultCode", kbPoint.getRes_code());
		if("S".equals(kbPoint.getResult())) {
			map.put("point", ""+kbPoint.getAvail_point());
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Get card point total Elapsed time : " + (System.currentTimeMillis() - start));
		
		return JSONValue.toJSONString(map);
	}
	
	@RequestMapping(value = "/pointSwap/minusKbPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String minusKbPoint(@RequestParam(required = false) String userCi,
			@RequestParam(required = true) int point, @RequestParam(required = true) int availPoint,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		logger.debug("minus point start!!! parameter userCi[" + userCi + "]");
		logger.debug("minus point start!!! parameter point[" + point + "]");
		logger.debug("minus point start!!! parameter availPoint[" + availPoint + "]");
		long start = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<>();
		KbCardMessage kbMsg = new KbCardMessage();
		
		if (userCi == null || "".equals(userCi)) {
			kbMsg.setUser_ci("TEST000012345678901234567890123456789012345678901234567890123456789012345678901234567890");		// 현업통해 전달받으면 입력 후 테스트
		} else {
			kbMsg.setUser_ci(userCi);
		}
		
		if (point <= 0 || availPoint <= 0) {
			map.put("result", "F");
			map.put("resultCode", "01");
			
			logger.debug("minus point start!!! point is invalid");
			
			return JSONValue.toJSONString(map);
		}
		
		kbMsg.setReq_point(point);
		kbMsg.setAvail_point(availPoint);
		Future<KbCardMessage> kbResult = kbCardClientService.minusPoint(kbMsg);
		
		logger.info("PointSwap Use WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		KbCardMessage kbPoint = kbResult.get();
		
		logger.info("PointSwap Use Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"], avail point["+kbPoint.getAvail_point() +"], use point["+kbPoint.getRes_point()+"]");
		logger.debug("PointSwap Use COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		map.put("result", kbPoint.getResult());
		map.put("resultCode", kbPoint.getRes_code());
		if("S".equals(kbPoint.getResult())) {
			map.put("point", ""+kbPoint.getAvail_point());
			map.put("usePoint", kbPoint.getRes_point()+"");
			map.put("approveNo", kbPoint.getApprove_no());
			map.put("transId", kbPoint.getTrans_id());
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Use card point total Elapsed time : " + (System.currentTimeMillis() - start));
		
		return JSONValue.toJSONString(map);
	}
	
	@RequestMapping(value = "/pointSwap/cancelMinusKbPoint.do", method = { RequestMethod.POST })
	@ResponseBody
	public String cancelMinusKbPoint(@RequestParam(required = false) String userCi,
			@RequestParam(required = true) String transId, @RequestParam(required = true) int point,
//			@RequestParam(required = true) int availPoint,
			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
		logger.debug("cancel minus point start!!! parameter userCi[" + userCi + "]");
		logger.debug("cancel minus point start!!! parameter transId[" + transId + "]");
		logger.debug("cancel minus point start!!! parameter point[" + point + "]");
//		logger.debug("cancel minus point start!!! parameter availPoint[" + availPoint + "]");
		long start = System.currentTimeMillis();
		HashMap<String, String> map = new HashMap<>();
		KbCardMessage kbMsg = new KbCardMessage();

		if (userCi == null || "".equals(userCi)) {
			kbMsg.setUser_ci("TEST000012345678901234567890123456789012345678901234567890123456789012345678901234567890");		// 현업통해 전달받으면 입력 후 테스트
		} else {
			kbMsg.setUser_ci(userCi);
		}
		
		if (transId == null || "".equals(transId)) {
			map.put("result", "F");
			map.put("resultCode", "02");
			
			logger.debug("cancel minus point start!!! transId is invalid");
			
			return JSONValue.toJSONString(map);
		}
		
		kbMsg.setTrans_id(transId);
		kbMsg.setReq_point(point);
//		kbMsg.setAvail_point(availPoint);
		Future<KbCardMessage> kbResult = kbCardClientService.cancelMinusPoint(kbMsg);
		
		logger.info("PointSwap Cancel WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		KbCardMessage kbPoint = kbResult.get();
		
		logger.info("PointSwap Cancel Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"], avail point["+kbPoint.getAvail_point() +"], use point["+kbPoint.getRes_point()+"]");
		logger.debug("PointSwap Cancel COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		map.put("result", kbPoint.getResult());
		map.put("resultCode", kbPoint.getRes_code());
		if("S".equals(kbPoint.getResult())) {
			map.put("point", ""+kbPoint.getAvail_point());
			map.put("usePoint", kbPoint.getRes_point()+"");
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Cancel card point total Elapsed time : " + (System.currentTimeMillis() - start));
		
		return JSONValue.toJSONString(map);
	}
}
