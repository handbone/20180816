/*[CLiP Point] version [v3.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.pointswap.dao;

import java.util.List;
import java.util.Map;

import com.coopnc.cardpoint.domain.PointSwapInfo;

import clip.integration.bean.PointSwapCancelVo;

public interface PointSwapDao {
	
	public int selectDailySerialNo(String systemCode) throws Exception;
	
	public int selectDailyApproveNo(String systemCode) throws Exception;
	
//	public Map<String, Integer> selectTodaySwapPoint(PointSwapInfo pointSwapInfo) throws Exception;
	public List<Map<String, Object>> selectTodaySwapPoint(Map<String, Object> pointSwapInfo) throws Exception;
	
	public List<PointSwapInfo> selectSwapList(PointSwapInfo pointSwapInfo) throws Exception;
	
	public PointSwapInfo selectSwapInfo(PointSwapInfo pointSwapInfo) throws Exception;
	
	public int insertPointSwapHist(PointSwapInfo pointSwapInfo) throws Exception;

	public List<PointSwapCancelVo> selectSwapCancelInfoList() throws Exception;
	
	public void updateSwapCancelInfo(PointSwapCancelVo param) throws Exception;
	
	public PointSwapInfo selectSwapHistForCancel(int idx) throws Exception;
	
	public PointSwapCancelVo selectSwapCancelInfo(int idx) throws Exception;
	
	public int insertPointSwapCancelInfo(PointSwapInfo pointSwapInfo) throws Exception;
	
	public PointSwapInfo selectSwapListForTransId(PointSwapInfo pointSwapInfo) throws Exception;
	
	public void updateSwapInfoApproveNo(PointSwapInfo param) throws Exception;
	
}
