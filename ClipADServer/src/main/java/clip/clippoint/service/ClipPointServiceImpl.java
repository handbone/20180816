package clip.clippoint.service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import clip.framework.BaseConstant;
import clip.framework.HttpNetwork;
import clip.roulette.bean.PointApicallLog;
import clip.roulette.bean.PointSwap;
import clip.roulette.dao.RouletteDao;

@Service("clipPointService")
public class ClipPointServiceImpl implements ClipPointService {
	Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private RouletteDao rouletteDao;	//로그용
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	public int procClipPoint(HttpServletRequest request, PointSwap param, int targetSystem, int inout, String tr_id, int point) throws Exception {

		String requester_code = "", systemName = "";
		String regSource = "";
		switch(targetSystem) {
			case 0 :
				requester_code = "pointswap_hana"; systemName = "하나멤버스";
				regSource = "hanamembers";
				break;
			case 1 :
				requester_code = "pointswap_kb"; systemName = "KB포인트리";
				regSource = "kbpointree";
				break;
			case 2 :
				requester_code = "pointswap_shinhan"; systemName = "신한카드";
				regSource = "shinhan";
				break;
			case 3 :
				requester_code = "pointswap_bc"; systemName = "BC카드";
				regSource = "bccard";
				break;
			default :
				throw new Exception("Invalid args");
		}

		StringBuffer log = new StringBuffer();
    	log.append("\r\n===================================================================\r\n");
    	Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy.MM.dd HH:mm:ss.SSS");
    	log.append("Request Time=" + format.format(calendar.getTime()) + "\r\n");
    	
    	String description = "["+systemName+"] 포인트 전환";

    	String localip = getLocalHost();
    	
    	String idString = "user_ci="+param.getUser_ci();
		
		// 통신 후 로그쌓을 준비
		PointApicallLog pointApicallLog = new PointApicallLog();
		pointApicallLog.setCust_id("");
		pointApicallLog.setCtn("");
		pointApicallLog.setGa_id("");
		pointApicallLog.setUser_ci(param.getUser_ci());
		pointApicallLog.setApiurl(BaseConstant.ADV_PLUS_POINT);
		pointApicallLog.setTr_id(tr_id);
		pointApicallLog.setTr_response("0");
		pointApicallLog.setIpaddr(localip);
		
		String endUrl = idString
						+"&transaction_id="+tr_id
						+"&requester_code=" + requester_code
						+"&point_value="+point
						+"&description="+java.net.URLEncoder.encode(description,BaseConstant.DEFAULT_ENCODING);
		endUrl = endUrl.replaceAll("(\r\n|\n)", "");
		endUrl = endUrl.replaceAll(System.getProperty("line.separator"), "");
		
		// Plus Point API 접속 URL
		logger.debug("Plus Point API URL : "+BaseConstant.ADV_PLUS_POINT+"?"+endUrl);
		
		String strHtmlSource = "";

		if(point == 0){
			strHtmlSource = "No Request.";
		} else {
			try { 
				// 실제 통신이 일어나는 부분
				strHtmlSource = httpNetwork.strGetData(BaseConstant.ADV_PLUS_POINT, endUrl);
				logger.debug("Plus Point API URL Response : "+strHtmlSource);
			} catch (Exception e ){
				pointApicallLog.setTr_response(e.getMessage());
				// 통신에서 에러가 발생했을때 에러 메시지를 로그로 남긴다
				logger.debug("Plus Point API URL Response : "+e.getMessage());
				if(pointApicallLog.getTr_response().contains("code: 600")){
					logger.debug("CLIPPOINTERROR 305:pointPlus http api error.");
	        	} else {
	        		logger.debug("CLIPPOINTERROR 300:pointPlus http api error.");
	        	}
			}
		}
		
    	log.append("Request URL:" + request.getRequestURL().toString() + "\r\n");
    	log.append("Plus Point API URL :" + BaseConstant.ADV_PLUS_POINT+ "\r\n");
    	log.append("Plus Point API Parameter [idString]:" + idString + "\r\n");
    	log.append("Plus Point API Parameter [transaction_id]:" + tr_id + "\r\n");
    	log.append("Plus Point API Parameter [requester_code]:" + requester_code + "\r\n");
    	log.append("Plus Point API Parameter [point_value]:" + point+ "\r\n");
    	log.append("Plus Point API Parameter [description]:" + description + "\r\n");
    	if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립(600)
    		log.append("Plus Point API Response :" + pointApicallLog.getTr_response() + "\r\n");
    	}else{
    		log.append("Plus Point API Response :" + strHtmlSource + "\r\n");
    	}
    	
    	//log.append("Request IP=" + request.getRemoteAddr() + "\r\n");
    	Calendar calendar2 = Calendar.getInstance();
    	log.append("Response Time=" + format.format(calendar2.getTime()) + "\r\n");
    	log.append("===================================================================\r\n");
        logger.info(log.toString());
        
        //연동로그 입력 : 성공시에는 Tr_response = 0
        rouletteDao.insertPointApicallLog(pointApicallLog);
        
        if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립
        	if(pointApicallLog.getTr_response().contains("code: 600")){ // 중복적립
        		logger.info("PlusPoint [" + regSource + "] Error : 중복적립으로 실패하였습니다.");
        		return 3;
        	}else{
        		logger.info("PlusPoint [" + regSource + "] Error : 통신실패로 실패하였습니다.");
        		return 0;
        	}
		} else { // 성공
			logger.info("PlusPoint [" + regSource + "] Success : Point[" + point + "]");
			return 1;
		}
	}
	
	private void plusClipPointCancel(){
		
	}
	
	private String getLocalHost() {
		String ip = "";

		try {
			InetAddress addr = InetAddress.getLocalHost();
			ip = addr.getHostAddress();
		} catch (UnknownHostException e1) {
			//e1.printStackTrace();
		}

		if(StringUtils.isNotEmpty(ip))
			return ip;
		
		try {
			boolean isLoopBack = true;
			Enumeration<NetworkInterface> en;		
			en = NetworkInterface.getNetworkInterfaces();

 			while(en.hasMoreElements()) {
				NetworkInterface ni = en.nextElement();
				if (ni.isLoopback())
					continue;

				Enumeration<InetAddress> inetAddresses = ni.getInetAddresses();
				while(inetAddresses.hasMoreElements()) { 
					InetAddress ia = inetAddresses.nextElement();
					if (ia.getHostAddress() != null && ia.getHostAddress().indexOf(".") != -1) {
						ip = ia.getHostAddress();
						System.out.println(ip);
						isLoopBack = false;
						break;
					}
				}
				if (!isLoopBack)
					break;
			}
		} catch (SocketException e) {
			e.printStackTrace();
			ip = "127.0.0.1";
		}
		
		return ip;
	}

	@Override
	public int plusPoint(String requester_code, String user_ci, String tr_id, int point, String description) throws Exception {
		String systemId = "", systemName = "";

		StringBuffer log = new StringBuffer();
    	log.append("\r\n===================================================================\r\n");
    	Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy.MM.dd HH:mm:ss.SSS");
    	log.append("Request Time=" + format.format(calendar.getTime()) + "\r\n");

    	String localip = InetAddress.getLocalHost().getHostAddress();
    	
    	String idString = "user_ci=" + user_ci;
		
		// 통신 후 로그쌓을 준비
		PointApicallLog pointApicallLog = new PointApicallLog();
		pointApicallLog.setCust_id("");
		pointApicallLog.setCtn("");
		pointApicallLog.setGa_id("");
		pointApicallLog.setUser_ci(user_ci);
		pointApicallLog.setApiurl(BaseConstant.ADV_PLUS_POINT);
		pointApicallLog.setTr_id(tr_id);
		pointApicallLog.setTr_response("0");
		pointApicallLog.setIpaddr(localip);
		
		String endUrl = idString
						+"&transaction_id="+tr_id
						+"&requester_code="+requester_code
						+"&point_value="+point
						+"&description="+java.net.URLEncoder.encode(description, BaseConstant.DEFAULT_ENCODING);
		endUrl = endUrl.replaceAll("(\r\n|\n)", "");
		endUrl = endUrl.replaceAll(System.getProperty("line.separator"), "");
		
		// Plus Point API 접속 URL
		logger.debug("Plus Point API URL : "+BaseConstant.ADV_PLUS_POINT+"?"+endUrl);
		
		String strHtmlSource = "";

		if(point == 0){
			strHtmlSource = "No Request.";
		} else {
			try { 
				// 실제 통신이 일어나는 부분
				strHtmlSource = httpNetwork.strGetData(BaseConstant.ADV_PLUS_POINT, endUrl);
				logger.debug("Plus Point API URL Response : "+strHtmlSource);
			} catch (Exception e ){
				pointApicallLog.setTr_response(e.getMessage());
				// 통신에서 에러가 발생했을때 에러 메시지를 로그로 남긴다
				logger.debug("Plus Point API URL Response : "+e.getMessage());
				if(pointApicallLog.getTr_response().contains("code: 600")){
					logger.debug("CLIPPOINTERROR 305:pointPlus http api error.");
	        	} else {
	        		logger.debug("CLIPPOINTERROR 300:pointPlus http api error.");
	        	}
			}
		}
		
    	log.append("Request URL: Clip AD \r\n");
    	log.append("Plus Point API URL :" + BaseConstant.ADV_PLUS_POINT+ "\r\n");
    	log.append("Plus Point API Parameter [idString]:" + idString + "\r\n");
    	log.append("Plus Point API Parameter [transaction_id]:" + tr_id + "\r\n");
    	log.append("Plus Point API Parameter [requester_code]:" + requester_code + "\r\n");
    	log.append("Plus Point API Parameter [point_value]:" + point+ "\r\n");
    	log.append("Plus Point API Parameter [description]:" + description + "\r\n");
    	if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립(600)
    		log.append("Plus Point API Response :" + pointApicallLog.getTr_response() + "\r\n");
    	}else{
    		log.append("Plus Point API Response :" + strHtmlSource + "\r\n");
    	}
    	
    	//log.append("Request IP=" + request.getRemoteAddr() + "\r\n");
    	Calendar calendar2 = Calendar.getInstance();
    	log.append("Response Time=" + format.format(calendar2.getTime()) + "\r\n");
    	log.append("===================================================================\r\n");
        logger.info(log.toString());
        
        //연동로그 입력 : 성공시에는 Tr_response = 0
        rouletteDao.insertPointApicallLog(pointApicallLog);
        
        if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립
        	if(pointApicallLog.getTr_response().contains("code: 600")){ // 중복적립
        		return 3;
        	}else{
        		return 0;
        	}
		} else { // 성공
			return 1;
		}
	}
	
	public class ClipPointRequest {
		
	}
}
