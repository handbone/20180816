/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.framework;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseConstant {
	/** Default Encoding (UTF-8) */
	public static final String	DEFAULT_ENCODING		= "utf-8";

	/** Default domain */
	//public static final String	DOMAIN					= "http://tclipps.giftishow.co.kr/";//"http://admin.fortune.daum.net:12005"; //"http://m.fortune.daum.net";
	public static final String	DOMAIN					= "http://clippointsvctb.clipwallet.co.kr/";//"http://admin.fortune.daum.net:12005"; //"http://m.fortune.daum.net";

	/** Default domain */
	public static final String	API_DOMAIN				= "http://clippointsvctb.clipwallet.co.kr/";
//	public static final String	API_DOMAIN				= "http://192.168.151.199:8180/";
	
	/** REQUESTER_CODE */
	public static final String	REQUESTER_CODE			= "clippoint"; //REQUESTER_CODE는 DB에 저장해놓고 사용하는 것으로 변경
	
	public static final String	REQUESTER_DESCRIPTION 	= "출석체크";
	/** API URL */
	public static final String	PLUS_POINT				= "plusPoint.do";
	
	/** 1등 당첨 시간대*/
	public static final String[] SELECTED_TIME	= {"9", "12", "15", "18", "20"};
	/** 1등 당첨자의 순서 ( 00번째 이후 ) */
	public static final String	SELECTED_TIME_NUM		= "3";
	
	/** LOCAL_IP_6 ) */
	public static final String	LOCAL_IP_6		= "0:0:0:0:0:0:0:1";
	
	
	/** svcId */
//	public static final String	SCV_ID				= "11652";

	/** codeName */
//	public static final String CODE_NAME				= "fortune_ver2";
	
	/**Service CODE*/
//	public static final String SCV_CODE = "ldHhGHglq7JpEmrWlztq3cvojniSK8vo9rsqrY1VwM8.";
	
	/** IMAGES SERVER*/
//	public static final String	IMAGES_SERVER			= "http://121.78.245.164";
	
	/** Success Code */
	public static final String	SUCCESS_CD				= "0";
	/** Success Code Name */
	public static final String	SUCCESS_NM				= "SUCCESS";
	
	/** System error code */
//	public static final String	SYSTEM_ERROR_CD			= "500";		
	
	
	;
	
	/** 로그인그룹확인 */
//	public static String AuthCheckPage(HttpServletRequest request) throws java.lang.Exception{
//		/*
//		 * 0 -- > 다음로그인 X
//		 * 1 -- > 다음로그인만 0
//		 * 2 -- > 세션정보 
//		 */				
//		
//		// 다음로그인정보있음
//		if((Object)request.getAttribute("userInfo") !=  null){
//			
//			//세션정보 없음
//			if(request.getSession().getAttribute("loginUser") == null){
//				return "1";
//			}else{
//				return "2";
//			}
//			
//		}else{
//			return "0";
//		}
//	} 
	
	
	/**********************************
	 *****       이하 고도화	  *****
	 **********************************/
	 
	//CLiP 포인트 서버 URL
	 public static final String ADV_GET_POINT = "/adv/getPoint.do";
	 public static final String ADV_PLUS_POINT = "/adv/plusPoint.do";
	 public static final String ADV_PLUS_POINT_CANCEL = "/adv/plusPointCancel.do";
	 public static final String ADV_MINUS_POINT = "/adv/minusPoint.do";
	 public static final String ADV_MINUS_POINT_CANCEL = "/adv/minusPointCancel.do";

	 //동의 여부 체크시 서비스 ID
	 public static final String AGREEMENT_SERVICE_ID_HANA = "hanamembers";
	 public static final String AGREEMENT_SERVICE_ID_KB = "kbcard";
	 public static final String AGREEMENT_SERVICE_ID_SHINHAN = "shinhancard";
	 public static final String AGREEMENT_SERVICE_ID_BC = "bccard";
	 
}
