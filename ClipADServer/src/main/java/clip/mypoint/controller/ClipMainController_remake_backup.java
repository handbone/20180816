//package clip.mypoint.controller;
//
//import java.util.Enumeration;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.log4j.Logger;
//import org.codehaus.plexus.util.StringUtils;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import clip.common.crypto.AES256CipherClip_Ext;
//import clip.framework.BaseController;
//import clip.integration.bean.ClipContract2Message;
//import clip.integration.service.ClipClientService;
//import clip.mypoint.service.MyPointService;
//
//@Controller
//public class ClipMainController_remake_backup extends BaseController {
//	Logger log = Logger.getLogger(this.getClass());
//	
//	@Resource(name="myPointService")
//	private MyPointService myPointService;
//	
//	@Resource(name="clipClientService")
//	private ClipClientService clipClientService;
//	
//	/** 포인트 메인 화면 **/
//	@RequestMapping(value="/pointmain/main.do")
//	public ModelAndView clipMain(HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointmain/main");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		handOverUserKey(request, mv);
//		
//		String pageType = (String) request.getParameter("pageType");
//		if(StringUtils.isNotEmpty(pageType)) {
//			mv.addObject("pageType", pageType);
//		}
//
//		
//		return mv;
//	}
//	
//	/*
//	포인트메인 모으기탭 : KTolleh00114://point/menu/?spec=gather
//	
//	포인트모으기(세부메뉴) :
//	
//	카드 포인트 가져오기 : KTolleh00114://point/menu/?spec=swap
//	참여 적립 : KTolleh00114://point/menu/?spec=offerwall
//	쇼핑 적립 : KTolleh00114://point/menu/?spec=shopbuy
//	금융 혜택 적립 : KTolleh00114://point/menu/?spec=fevent
//	포인트 쿠폰 등록 : KTolleh00114://point/menu/?spec=pcoupon&no={쿠폰번호}
//	
//	포인트메인 사용하기탭 : KTolleh00114://point/menu/?spec=use
//	
//	포인트사용하기(세부메뉴)
//	
//	모바일 쿠폰 구매 : KTolleh00114://point/menu/?spec=giftshow
//	오프라인 간편 결제 : KTolleh00114://point/menu/?spec=pointpay
//	VIP Zone : KTolleh00114://point/menu/?spec=viplounge
//	포인트메인 이용내역탭 : KTolleh00114://point/menu/?spec=history
//	*/
//	
//	/**
//	 * clip 메뉴별 직링크
//	 * @param request
//	 * @param response
//	 * @return
//	 * @throws java.lang.Exception
//	 */
//	@RequestMapping(value="/directLink.do")
//	public String directLink(RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
//
//		@SuppressWarnings("unchecked")
//		Enumeration<String> en = request.getParameterNames();
//		
//		String spec = (String) request.getParameter("spec");
//		String no = (String) request.getParameter("no");
//		
//		if(StringUtils.isNotEmpty(no)) 
//			no = AES256CipherClip_Ext.AES_Decode(no);
//		
//		log.debug("spec="+spec+"&no="+no);
//		System.out.println();
//		
//		StringBuffer buf = new StringBuffer();
//		boolean isFirst = true;
//        for ( ; en.hasMoreElements() ; )
//        {
//        	String name = en.nextElement();
//        	
//        	if(name.equals("spec")) {
//        		redirectAttributes.addFlashAttribute(name, (String)request.getParameter(name));
//        		continue;
//        	}
//        	
//        	if(name.equals("no") && StringUtils.isNotEmpty(no)) {
//        		redirectAttributes.addFlashAttribute(name, no);
//        		continue;
//        	}
//        	
//        	String encoded = java.net.URLEncoder.encode((String)request.getParameter(name), "UTF-8");
//        	
//        	if(isFirst) {
//        		buf.append("?"+name+"="+encoded);
//        		isFirst = false;
//        	} else {
//        		buf.append("&"+name+"="+encoded);
//        	}
//        }
//
//		return "redirect:/pointmain/main.do"+buf.toString();
//	}
//	
//	@RequestMapping(value = "/contractCheck.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
//	@ResponseBody
//	public ModelAndView contractCheck(@RequestBody ClipContract2Message param,
//			HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception {
//
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		
//		mav.addObject("result", "S");
//		mav.addObject("contractInfo", clipClientService.getContract2(param));
//
//		return mav;
//	}
//}
