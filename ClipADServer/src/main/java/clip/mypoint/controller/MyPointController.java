/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.mypoint.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import clip.framework.BaseController;
import clip.mypoint.bean.BannerInfoBean;
import clip.mypoint.bean.MyPointBean;
import clip.mypoint.service.MyPointService;

@Controller
@RequestMapping("/mypoint")
public class MyPointController extends BaseController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="myPointService")
	private MyPointService myPointService;
	
	/**마이포인트 화면 호출 **/
	@RequestMapping(value="/myPoint.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/myPoint");	
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");
		
		if(cust_id != null && gaid != null){
			
			mv.addObject("cust_id", cust_id.replaceAll("[\r\n]", ""));
			if(null != ctn && !"".equals(ctn)){
				mv.addObject("ctn", ctn.replaceAll("[\r\n]", ""));	
			}
			mv.addObject("gaid", gaid.replaceAll("[\r\n]", ""));
			
			if(user_ci != null){
				mv.addObject("user_ci", user_ci.replaceAll("[\r\n]", ""));
			}
			
			if(offerwall != null){
				mv.addObject("offerwall", offerwall.replaceAll("[\r\n]", ""));
			}
			
		}
		
		return mv;
	}
	
	/**마이포인트 화면 호출을 복사해서 만든 오퍼월 진입 테스트 URL **/
	@RequestMapping(value="/eventPoint.do")
	public ModelAndView eventPoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/eventPoint");	
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");
		
		if(cust_id != null && ctn != null && gaid != null){
			
			mv.addObject("cust_id", cust_id.replaceAll("[\r\n]", ""));
			mv.addObject("ctn", ctn.replaceAll("[\r\n]", ""));
			mv.addObject("gaid", gaid.replaceAll("[\r\n]", ""));
			
			if(user_ci != null){
				mv.addObject("user_ci", user_ci.replaceAll("[\r\n]", ""));
			}
			
			if(offerwall != null){
				mv.addObject("offerwall", offerwall.replaceAll("[\r\n]", ""));
			}
			
		}
		
		return mv;
	}
	

	/**user_ci 조회, user_token 조회 추가 **/
	@RequestMapping(value="/getUserCi.do")
	public void getUserCi(HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception{
		// JsonType formData
		MyPointBean myPointBean = (MyPointBean) getObjectFromJSON(request, MyPointBean.class);
		MyPointBean result = myPointService.getUserCi(myPointBean);

		if(StringUtils.isNotEmpty(result.getUser_ci()))
			result.setUser_token(myPointService.getUserToken(result.getUser_ci()));
		
		this.outputJSON(request, response, result);
	}
	
	/**사용가능 포인트 조회**/
	@RequestMapping(value="/getPoint.do")
	public void getPoint(HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception{
		// JsonType formData
		MyPointBean myPointBean = (MyPointBean) getObjectFromJSON(request, MyPointBean.class);
		this.outputJSON(request, response, myPointService.getPoint(myPointBean));
	}

	/**포인트 이력 조회**/
	@RequestMapping(value="/getPointHistory.do")
	public void getPointHistory(HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception{
		// JsonType formData
		MyPointBean myPointBean = (MyPointBean) getObjectFromJSON(request, MyPointBean.class);
		this.outputJSON(request, response, myPointService.getPointHistory(myPointBean));
	}
	
	/**포인트 이력 조회**/
//	@RequestMapping(value="/getPointHistoryNew.do")
//	public void getPointHistoryNew(HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception{
//		// JsonType formData
//		MyPointBean myPointBean = (MyPointBean) getObjectFromJSON(request, MyPointBean.class);
//		this.outputJSON(request, response, myPointService.getPointHistoryNew(myPointBean));
//	}
	
	/**포인트 이력 조회**/
	@RequestMapping(value="/getPointHistoryNew.do")
	@ResponseBody
	public void getPointHistoryNew(HttpServletRequest request, HttpServletResponse response,MyPointBean myPointBean) throws java.lang.Exception{
		// JsonType formData
		log.info(myPointBean);
		this.outputJSON(request, response, myPointService.getPointHistoryNew(myPointBean));
		
	}
	
	
	/**배너정보 조회**/
	@RequestMapping(value="/getBannerInfo.do")
	public void getBannerInfo(HttpServletRequest request, HttpServletResponse response) throws java.lang.Exception{
		// JsonType formData
		BannerInfoBean bannerInfoBean = (BannerInfoBean) getObjectFromJSON(request, BannerInfoBean.class);
		this.outputJSON(request, response, myPointService.getBannerInfo(bannerInfoBean));
	}

}
