package clip.mypoint.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import clip.common.util.ClipRequesterCode;
import clip.common.util.TelegramUtil;
import clip.integration.bean.ClipPointMessage;
import clip.integration.bean.ext.CoopPointCouponMessage;
import clip.integration.dao.PointCouponDao;
import clip.integration.service.ClipPointClientService;
import clip.integration.service.CoopClientService;
import clip.mypoint.bean.CouponState;
import clip.mypoint.bean.PointCouponBean;

@Service("pointCouponService")
public class PointCouponServiceImpl implements PointCouponService {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private CoopClientService coopClientService;
	
	@Autowired
	private ClipPointClientService clipPointClientService;
	
	@Autowired
	private ClipRequesterCode clipRequesterCode;
	
	// [jaeheung] 2017.08. 07 쿠폰 관련 DAO 추가
	@Autowired
	private PointCouponDao pointCouponDao;
	
	private static int SERIAL_NO;
	
	@Override
	public PointCouponBean checkCoupon(PointCouponBean param, HttpServletRequest request) throws Exception {
		
		CoopPointCouponMessage coopParam = new CoopPointCouponMessage();
		coopParam.setCouponNo(param.getCoupon_no());
		CoopPointCouponMessage coopResult = coopClientService.pointCouponCheck(coopParam, request);
		
		// [jaeheung] 2017.08.07 쿠폰 등록 결과 로그 정보 담을 Map 초기화
		Map<String, Object> msg = new HashMap<String, Object>();
		msg.put("userCi", request.getParameter("user_ci").toString());
		msg.put("balance", coopResult.getUse_price());
		msg.put("couponNumber", param.getCoupon_no());
		msg.put("couponName", coopResult.getCoupon_name());
		msg.put("resultCode", coopResult.getResultcode());
		msg.put("resultMsg", coopResult.getResultMsg());
		
		// [jaeheung] 2017.08.08 쿠폰 인증 성공 시 S 반환
		if("S".equals(coopResult.getResult())) {
			ClipPointMessage clipResult = null;
			try {
				
				ClipPointMessage clipParam = new ClipPointMessage();
				clipParam.setUser_ci(param.getUser_ci());
				clipParam.setTr_id(TelegramUtil.makeTrIdForClip(getSerial()));
				clipParam.setReg_source(clipRequesterCode.REQ_PCOUPON);
				clipParam.setPoint(coopResult.getUse_price());
				clipParam.setDescription("포인트 쿠폰 등록");
				
				clipResult = clipPointClientService.plusPoint(clipParam);
			} catch (Exception e){
				log.error(e.getMessage());
			}
			
			if("S".equals(clipResult.getResult())) {
				param.setResult("S");
				param.setResultMsg(coopResult.getResultMsg());
				param.setCoupon_value(""+coopResult.getUse_price());
				param.setPoint_value(clipResult.getBalance());
				
				// [jaeheung] 2017.08.07 적립 완료 후 결과 파라미터 값 저장
				msg.put("resultCode", "00");
				msg.put("resultMsg", clipResult.getResultMsg());
			} else {
				String orgTrId = coopResult.getTr_id();
				coopResult.setCouponNo(param.getCoupon_no());
				coopResult = coopClientService.pointCouponCancel(coopResult);
				
				param.setResult("F");
				param.setResultMsg("처리도중 오류가 발생하였습니다.");
				
				// [jaeheung] 2017.08.07 적립 실패 후 결과 파라미터 값 저장
				msg.put("resultCode", "92");
				msg.put("resultMsg", clipResult.getResultMsg());
			}
		}
		// [jaeheung] 2017.08.08 쿠폰 인증 성공 후 결과코드가 00이 아니면 F 반환
		else {
			param.setResult("F");
			if(coopResult.getResult() == "FF") {
				param.setResultMsg(coopResult.getResultMsg());
			} else {				
				param.setResultMsg("처리도중 오류가 발생하였습니다.");
			}
		}
		
		// [jaeheung] 쿠폰 인증 및 포인트 적립 결과 DB에 저장
		pointCouponDao.insertCouponHistory(msg);
		
		param.setErrorcode(coopResult.getResultcode());   // lee

		return param;
	}
	
	private synchronized static String getSerial(){
		
		if(SERIAL_NO < 1000)
			SERIAL_NO = 1000;

		int temp = ++SERIAL_NO;
		if(temp == 10000)
			SERIAL_NO = 1000;
		
		return ""+temp;
	}

	@Override
	public CouponState checkFailCouponCheck(String user_ci) throws Exception {
		return pointCouponDao.selectFailCouponCheck(user_ci);
	}

	@Override
	public void changeCouponBlock(CouponState obj) throws Exception {
		pointCouponDao.updateCouponBlock(obj);
	}

	@Override
	public void putCouponUserInfo(String user_ci) throws Exception {
		pointCouponDao.insertCouponBlock(user_ci);
	}
	
}
