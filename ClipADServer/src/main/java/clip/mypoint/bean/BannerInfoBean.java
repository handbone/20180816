/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.mypoint.bean;

import java.util.List;

public class BannerInfoBean {
	
	private String bannername;
	private String bannerimage;
	private String linktype;
	private String linkurl;
	private String androidgbn;
	private String iosgbn;
	
	private List<BannerInfoBean> bannerInfoList;

	public String getBannername() {
		return bannername;
	}

	public void setBannername(String bannername) {
		this.bannername = bannername;
	}

	public String getBannerimage() {
		return bannerimage;
	}

	public void setBannerimage(String bannerimage) {
		this.bannerimage = bannerimage;
	}

	public String getLinktype() {
		return linktype;
	}

	public void setLinktype(String linktype) {
		this.linktype = linktype;
	}

	public String getLinkurl() {
		return linkurl;
	}

	public void setLinkurl(String linkurl) {
		this.linkurl = linkurl;
	}

	public List<BannerInfoBean> getBannerInfoList() {
		return bannerInfoList;
	}

	public void setBannerInfoList(List<BannerInfoBean> bannerInfoList) {
		this.bannerInfoList = bannerInfoList;
	}

	public String getAndroidgbn() {
		return androidgbn;
	}

	public void setAndroidgbn(String androidgbn) {
		this.androidgbn = androidgbn;
	}

	public String getIosgbn() {
		return iosgbn;
	}

	public void setIosgbn(String iosgbn) {
		this.iosgbn = iosgbn;
	}
	

}
