/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.mypoint.bean;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.json.simple.JSONObject;

public class MyPointBean {
	private String description;
	private String datetime;
	private String point_type;
	private String point_value;
	private String balance;
	private String cust_id;
	private String ctn;
	private String gaid;
	private String mid;
	private String user_ci;
	private String start_date;
	private String end_date;
	
	private String resResult;
	
	/** 현재 페이지 번호 (0 부터 시작) */
	private String page_num;
	
	/**한번에 보여줄 리스트 수  */
	private String page_size;
	
	/**  point history  리스트 갯수   */
	private String page_list_size;
	/**
	 * 사용자 구분키 user_token 추가
	 */
	private String user_token;
	
	private String app_ver;
	
	private List<JSONObject> saveList;
	private List<JSONObject> useList;
	
	private List<MyPointBean> myPointList;
	
	//적립/차감, 토탈 리스트 구분
	private String listType;
	private List<JSONObject> histList;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getPoint_type() {
		return point_type;
	}

	public void setPoint_type(String point_type) {
		this.point_type = point_type;
	}

	public String getPoint_value() {
		return point_value;
	}

	public void setPoint_value(String point_value) {
		this.point_value = point_value;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getCtn() {
		return ctn;
	}

	public void setCtn(String ctn) {
		this.ctn = ctn;
	}

	public String getGaid() {
		return gaid;
	}

	public void setGaid(String gaid) {
		this.gaid = gaid;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getUser_ci() {
		return user_ci;
	}

	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getResResult() {
		return resResult;
	}

	public void setResResult(String resResult) {
		this.resResult = resResult;
	}

	public List<JSONObject> getSaveList() {
		return saveList;
	}

	public void setSaveList(List<JSONObject> saveList) {
		this.saveList = saveList;
	}

	public List<JSONObject> getUseList() {
		return useList;
	}

	public void setUseList(List<JSONObject> useList) {
		this.useList = useList;
	}

	public List<MyPointBean> getMyPointList() {
		return myPointList;
	}

	public void setMyPointList(List<MyPointBean> myPointList) {
		this.myPointList = myPointList;
	}
	public String getApp_ver() {
		return app_ver;
	}

	public void setApp_ver(String app_ver) {
		this.app_ver = app_ver;
	}

	public String getUser_token() {
		return user_token;
	}

	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}

	public String getListType() {
		return listType;
	}

	public void setListType(String listType) {
		this.listType = listType;
	}

	public List<JSONObject> getHistList() {
		return histList;
	}

	public void setHistList(List<JSONObject> histList) {
		this.histList = histList;
	}
	
	public String getPage_num() {
		return page_num;
	}

	public void setPage_num(String page_num) {
		this.page_num = page_num;
	}

	public String getPage_size() {
		return page_size;
	}

	public void setPage_size(String page_size) {
		this.page_size = page_size;
	}
	@Override
	public String toString() {
		
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}

	public String getPage_list_size() {
		return page_list_size;
	}

	public void setPage_list_size(String page_list_size) {
		this.page_list_size = page_list_size;
	}


}