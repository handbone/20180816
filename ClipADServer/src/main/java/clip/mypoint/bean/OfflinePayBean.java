package clip.mypoint.bean;

public class OfflinePayBean {
	
	private String user_token;
	private String pin_id;
	private String couponId;
	
	
	
	private String item_id;
	private String item_name;
	private String item_image_url;
	private String comp_image_url;
	private String barcode_image_url;
	private String description;
	
	
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getUser_token() {
		return user_token;
	}
	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getComp_image_url() {
		return comp_image_url;
	}
	public void setComp_image_url(String comp_image_url) {
		this.comp_image_url = comp_image_url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getItem_image_url() {
		return item_image_url;
	}
	public void setItem_image_url(String item_image_url) {
		this.item_image_url = item_image_url;
	}
	public String getBarcode_image_url() {
		return barcode_image_url;
	}
	public void setBarcode_image_url(String barcode_image_url) {
		this.barcode_image_url = barcode_image_url;
	}
	public String getPin_id() {
		return pin_id;
	}
	public void setPin_id(String pin_id) {
		this.pin_id = pin_id;
	}
	
}
