package clip.mypoint.dao;

import java.util.List;

import clip.mypoint.bean.OfflinePayBean;

public interface OfflinePayDao {
	
	/**
	 * 상품 리스트 조회
	 * @param param
	 * @return
	 */
	public List<OfflinePayBean> selectOfflinePayItemList();
	
	/**
	 * 상품별 상세 정보 조회
	 * @param param
	 * @return
	 */
	public OfflinePayBean selectOfflinePayItem(OfflinePayBean param);
	
}
