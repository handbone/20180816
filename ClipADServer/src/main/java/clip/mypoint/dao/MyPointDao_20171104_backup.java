///*[CLiP Point] version [v1.0]
//* Copyright © 2016 kt corp. All rights reserved.
//* This is a proprietary software of kt corp, and you may not use this file except in
//* compliance with license agreement with kt corp. Any redistribution or use of this
//* software, with or without modification shall be strictly prohibited without prior written
//* approval of kt corp, and the copyright notice above does not evidence any actual or
//* intended publication of such software.
//*/
//package clip.mypoint.dao;
//
//import java.util.List;
//
//import clip.mypoint.bean.BannerInfoBean;
//
//public interface MyPointDao_20171104_backup {
//
//	List<BannerInfoBean> getBannerInfo(BannerInfoBean bannerInfoBean) throws java.lang.Exception;
//
//	String getUserToken(String userCi) throws java.lang.Exception;
//	
//	int insertUserToken(String userCi, String userToken) throws java.lang.Exception;
//	
//}
