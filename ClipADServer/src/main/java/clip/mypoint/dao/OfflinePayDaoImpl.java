package clip.mypoint.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import clip.mypoint.bean.OfflinePayBean;

@Repository("offlinePayDao")
public class OfflinePayDaoImpl implements OfflinePayDao {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;

	@Override
	public List<OfflinePayBean> selectOfflinePayItemList() {
		return sqlSession.selectList("OfflinePay.selectOfflinePayItemList");
	}

	@Override
	public OfflinePayBean selectOfflinePayItem(OfflinePayBean param) {
		return sqlSession.selectOne("OfflinePay.selectOfflinePayItem", param);
	}
	
}
