package clip.integration.service;

import java.util.concurrent.Future;

import clip.integration.bean.HanaCardMessage;

public interface HanaCardClientService {

	//포인트 조회
	public Future<HanaCardMessage> getPoint(HanaCardMessage param);
	
	//포인트 차감
	public Future<HanaCardMessage> minusPoint(HanaCardMessage param);
	
	//포인트 차감 취소
	public Future<HanaCardMessage> cancelMinusPoint(HanaCardMessage param);

	//일대사
	public int calculateHanaPointSwap() throws Exception;
	public int calculateHanaPointSwap(String preDate) throws Exception;
	
}
