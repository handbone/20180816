package clip.integration.service;

import javax.servlet.http.HttpServletRequest;

import clip.integration.bean.CoopMessage;
import clip.integration.bean.ext.CoopPointCouponMessage;

public interface CoopClientService {

	//쿠폰발급
	public CoopMessage issueCoupon(CoopMessage param);

	//바코드요청
	public CoopMessage getBarcodeUrl(CoopMessage param);

	//포인트 쿠폰 사용
	public CoopPointCouponMessage pointCouponCheck(CoopPointCouponMessage param, HttpServletRequest request) throws Exception;
	
	//포인트 쿠폰 사용 취소 (망취소(
	public CoopPointCouponMessage pointCouponCancel(CoopPointCouponMessage param) throws Exception;

}
