package clip.integration.service;

import java.util.List;

import clip.integration.bean.LinkPriceReportMessage;
import clip.integration.bean.LinkPriceItem;
import clip.integration.bean.LinkPriceTransMessage;

public interface LinkPriceClientService_20171104_backup {
	
	//익월적립조회
	public LinkPriceReportMessage getNextSaving(LinkPriceReportMessage param);
	
	//익월적립내역
	public LinkPriceReportMessage getNextSavingHistory(LinkPriceReportMessage param);
	
	//익익월적립조회
	public LinkPriceReportMessage getNext2Saving(LinkPriceReportMessage param);
	
	//익익월적립내역
	public LinkPriceReportMessage getNext2SavingHistory(LinkPriceReportMessage param);
	
	//실적조회
	public LinkPriceTransMessage getTransSaving(LinkPriceTransMessage param);

	//Shop 목록 조회
	public List<LinkPriceItem> getItemList(LinkPriceItem param);

	//Shop 상세 조회
	public LinkPriceItem getItemInfo(LinkPriceItem param);
	
}
