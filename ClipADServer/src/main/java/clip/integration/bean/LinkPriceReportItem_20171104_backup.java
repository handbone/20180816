package clip.integration.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceReportItem_20171104_backup {
	
	//광고주명
	private String merchant_name;
	//결제 날짜
	private String yyyymmdd;
	//결제 시간
	private String hhmiss;
	//결제 금액
	private String sales;
	//적립예정 포인트
	private String commission;

	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public String getHhmiss() {
		return hhmiss;
	}
	public void setHhmiss(String hhmiss) {
		this.hhmiss = hhmiss;
	}
	public String getSales() {
		return sales;
	}
	public void setSales(String sales) {
		this.sales = sales;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getMerchant_name() {
		return merchant_name;
	}
	public void setMerchant_name(String merchant_name) {
		this.merchant_name = merchant_name;
	}		
}