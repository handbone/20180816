package clip.integration.bean;

import com.coopnc.cardpoint.domain.PointSwapInfo;

public class ShinhanCardMessage extends PointSwapInfo {
	private boolean isPointOver;

	public boolean isPointOver() {
		return isPointOver;
	}

	public void setPointOver(boolean isPointOver) {
		this.isPointOver = isPointOver;
	}
}
