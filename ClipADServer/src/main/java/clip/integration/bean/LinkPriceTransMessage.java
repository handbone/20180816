package clip.integration.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceTransMessage {
	//조회일자
	private String yyyymmdd;
	
	//응답코드 0-조회 성공, 100-어필 아이디 없음, 200-조회일자 없음, 300-인증키가 맞지 않음
	private String result;
	//실적조회갯수
	private String listCount;
	//실적조회목록
	private List<LinkPriceTransItem> transItemList;
	
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getListCount() {
		return listCount;
	}
	public void setListCount(String listCount) {
		this.listCount = listCount;
	}
	public List<LinkPriceTransItem> getTransItemList() {
		return transItemList;
	}
	public void setTransItemList(List<LinkPriceTransItem> transItemList) {
		this.transItemList = transItemList;
	}

	public String getParameters() {
		StringBuffer sb = new StringBuffer();
		sb.append("&yyyymmdd=");
		sb.append(this.yyyymmdd);
		return sb.toString();
	}

}