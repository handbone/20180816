package clip.integration.bean;

public class PointSwapCancelVo {
	
	private int idx;
	private String reg_source;
	private String reg_date;
	private int old_swap_idx;
	private int new_swap_idx;
	private int status;
	private String regdate;
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getReg_source() {
		return reg_source;
	}
	public void setReg_source(String reg_source) {
		this.reg_source = reg_source;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public int getOld_swap_idx() {
		return old_swap_idx;
	}
	public void setOld_swap_idx(int old_swap_idx) {
		this.old_swap_idx = old_swap_idx;
	}
	public int getNew_swap_idx() {
		return new_swap_idx;
	}
	public void setNew_swap_idx(int new_swap_idx) {
		this.new_swap_idx = new_swap_idx;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
