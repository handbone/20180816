package clip.schedule;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import clip.integration.service.BcCardClientService;
import clip.integration.service.HanaCardClientService;
import clip.pointswap.service.PointSwapService;

@Controller
@RequestMapping(value ="/schedule")
public class Schedule {
	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['HAHA_BATCH_USE_YN']}") private String HAHA_BATCH_USE_YN;
	
	@Value("#{props['CANCEL_BATCH_USE_YN']}") private String CANCEL_BATCH_USE_YN;
	
	@Value("#{props['BC_BATCH_USE_YN']}") private String BC_BATCH_USE_YN;
	
	@Autowired
	private HanaCardClientService hanaCardClientService;
	
	@Autowired
	private PointSwapService pointSwapService;
	
	@Autowired
	private BcCardClientService bcCardClientService;
	
	/**
	 * 하나 포인트 스왑 일대사 배치
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception  
	 */
	@Scheduled(cron="0 0 2 * * *")//초 분 시 일 월 주(년) , 매일 2시 0분에 실행
	public void calculateHanaPointSwap() throws Exception {
		if("Y".equals(HAHA_BATCH_USE_YN)) {
			logger.debug("calculateHanaPointSwap start!!!");
			hanaCardClientService.calculateHanaPointSwap();
		} else {
			logger.debug("calculateHanaPointSwap is OFF");
		}
	}
	
	/**
	 * 하나 포인트 스왑 일대사 배치 즉시 실행
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value ="/calculateHanaPointSwap.do")
	@ResponseBody
	public ResponseEntity<String> calculateHanaPointSwap(
			@RequestParam(required = true) String preDate, HttpServletRequest request, ModelMap model) throws Exception{
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		logger.debug("calculateHanaPointSwap ["+preDate+"] start!!!");
		hanaCardClientService.calculateHanaPointSwap(preDate);
		
		return new ResponseEntity<String>("calculateHanaPointSwap complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/**
	 * 포인트 스왑 취소 배치
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception  
	 */
	@Scheduled(cron="0 */10 * * * *")//초 분 시 일 월 주(년) , 10분마다 실행
	public void pointSwapCancel() throws Exception {
		if("Y".equals(CANCEL_BATCH_USE_YN)) {
			logger.debug("pointSwapCancel start!!!");
			int result = pointSwapService.pointSwapCancelBatch();
			logger.debug("pointSwapCancel end!!! ["+result+"]");
		} else {
			logger.debug("pointSwapCancel is OFF");
		}
	}
	
	/**
	 * 포인트 스왑 취소 배치 즉시 실행
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value ="/pointSwapCancel.do")
	@ResponseBody
	public ResponseEntity<String> pointSwapCancel(HttpServletRequest request, ModelMap model) throws Exception{
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		logger.debug("pointSwapCancel start!!!");
		pointSwapService.pointSwapCancelBatch();
		logger.debug("pointSwapCancel end!!!");
		
		return new ResponseEntity<String>("pointSwapCancel complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/**
	 * 비씨 포인트 스왑 일대사 배치 즉시 실행
	 * @param preDate
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value ="/calculateBcPointSwap.do")
	@ResponseBody
	public ResponseEntity<String> calculateBcPointSwap(
			@RequestParam(required = true) String preDate, HttpServletRequest request, ModelMap model) throws Exception{
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		logger.debug("calculateBcPointSwap ["+preDate+"] start!!!");
		bcCardClientService.calculateBcPointSwap(preDate);
		
		return new ResponseEntity<String>("calculateBcPointSwap complete",responseHeaders, HttpStatus.CREATED);
	}
	
	/**
	 * 비씨 포인트 스왑 일대사 받은 파일 프로세싱
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value ="/processBcPointSwap.do")
	@ResponseBody
	public ResponseEntity<String> processBcPointSwap(
			@RequestParam(required = false) String preDate, HttpServletRequest request, ModelMap model) throws Exception{
		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		logger.debug("processBcPointSwap start!!!");
		bcCardClientService.processBcPointSwap(preDate);
		
		return new ResponseEntity<String>("processBcPointSwap complete",responseHeaders, HttpStatus.CREATED);
	}
	
	@Scheduled(cron="0 30 1 * * *")//초 분 시 일 월 주(년) , 1시 30분마다 실행
	public void schedeulBcPointSwap() throws Exception {
		if("Y".equals(BC_BATCH_USE_YN)) {
			logger.debug("schedeulBcPointSwap start!!!");
			int result = bcCardClientService.processBcPointSwap(null);
			logger.debug("schedeulBcPointSwap end!!! ["+result+"]");
		} else {
			logger.debug("schedeulBcPointSwap is OFF");
		}
	}
}
