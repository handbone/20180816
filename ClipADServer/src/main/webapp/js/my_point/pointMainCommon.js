/**
 * 약관 동의 요청 결과 확인 함수
 * 
 * @author jaeheung
 * @date 2017.08.10
 * @param result	- 약관 동의 결과 값
 * @param groupCode	- 약관 동의 그룹 코드
 */
function onResultTermAgree(result, groupCode) {
	
	if(result == 'ok') {
		if(groupCode == 'GRTRM00001') {
			movePage('/clip/pointcollect/pointSwapMain.do', 'form');
		} else if(groupCode == 'GRTRM00002') {
			movePage('/clip/pointcollect/shoppingMain.do', 'form');
		} else {
			return;
		}
	}
	else if(result == 'cancel') {
		// TODO
		return;
	}
}
var myPoint = {
		 fn: {
					/**콤마 넣기**/
					 addComma : function (n) {
					    var reg = /(^[+-]?\d+)(\d{3})/;
					    n += '';
					
					    while (reg.test(n))
					        n = n.replace(reg, '$1' + ',' + '$2');
					    return n;
					},


					moveUrl : function(linktype, linkurl){
					
					var paramId = "";
					var startUrl = "";
					var endUrl = "";
					var resultUrl = "";
					var paramVal = "";
					
					if(linktype == "http"){
						
						var chkStr = linkurl.indexOf('{');
						
						if(chkStr == -1){
							// AS-IS
				//			location.href = linkurl;
							// TO-BE
				//			window.location = 'KTolleh00114://gobrowser?'+linkurl;r
							window.location = linkUrl;
						}else{
							var tempArray1 = linkurl.split('{');
							var tempArray2 = tempArray1[1].split('}');
							paramId = tempArray2[0];
							startUrl = tempArray1[0];
							endUrl = tempArray2[1];
							
							paramVal = $("#"+paramId).val();		
							resultUrl = startUrl+encodeURIComponent(paramVal)+endUrl;
							
							if ( $("#gaid").length > 0 && $("#gaid").val() != '' ) {
								resultUrl = resultUrl + "&ga_id=" +$("#gaid").val();
							}
							
							// AS-IS
							// location.href = resultUrl;
							// TO-BE
				//			window.location = 'KTolleh00114://gobrowser?' + resultUrl;
							window.location = resultUrl;
						}
						
					}else{
						//스크립트 호출
						linkurl;
					}
				},
			   	//제3자 약관동의 여부 체크
		       	getContractCheck : function (obj){
		       		
		       		var contCode = "";
		       		if(obj == 'pointswap'){
		       			contCode = 'GRTRM00001';
		       		} else if (obj == 'shopbuy') {
		       			contCode = 'GRTRM00002';
		       		} else {
		       			return false;
		       		}
		       		
		       		var cust_id = $("#cust_id").val();
		       		var paramData = {
		       				"custId"				: cust_id,
		       				"serviceName"				: obj
		       		};
		       		$.ajax({
							url		: "/clip/contractCheck.do",
					    	type: "POST",
					        data: JSON.stringify(paramData),
					        dataType: "json",
					        contentType: 'application/json; charset=UTF-8',
							success	: function(resJSON, resCode) {
								
								var contractResult = resJSON.contractInfo.result;
								var contractShowYn = resJSON.contractInfo.contractShowYn;
								var contractGroupCode = resJSON.contractInfo.groupCode;
								if(contractResult == 'F') {
//									alert("서버 오류입니다.");
									// 180119 jyi 추가
							    		var _text = '서버 오류입니다.';
									$('#notice_word').empty();
									$('#notice_word').append(_text);
									$('#popup_type').val('default');
									$('#alert_type').hide();
									$('#alert_popup').show();
									// //180119 jyi 추가
								} else {
									if(contractShowYn == 'Y') {
										// TODO 약관동의 웹뷰 연동 진행 중, 연동 완료 후 주석 제거
										window.location='webtoapp://clippoint/termagree?groupCode='+contCode;
										//alert('현재 약관 동의 화면 연동 진행중');
										//onResultTermAgree('OK', contCode);
									} else {
										if(obj == 'pointswap') {
											movePage('/clip/pointcollect/pointSwapMain.do', 'form');
										}
										if(obj == 'shopbuy') {
											movePage('/clip/pointcollect/shoppingMain.do', 'form');
										}
									}
								}
							},
							beforeSend : function() {
								loadingClip.openClip();
							},
							complete : function() {
								loadingClip.closeClip();
							}
							
						});
		    		
		    	}
				
			   	/**포인트 이력 조회**/
		    	
		    ,	getPointHistory : function(){
		    		var user_ci = $("#user_ci").val();
		    		
		    		var d = new Date();
		    		d.setMonth(d.getMonth()+1);
		        		var end_date = myPoint.fn.leadingZeros(d.getFullYear(), 4) + myPoint.fn.leadingZeros(d.getMonth(), 2) + myPoint.fn.leadingZeros(d.getDate(), 2);
		    		
		    		d.setMonth(d.getMonth()-2);
		    		var syyyy, sm, sd;
		    		syyyy = d.getFullYear();
		    		sm = d.getMonth();
		    		sd = d.getDate();
		    		if (sm == "0") { sm="12"; syyyy=syyyy-1; }
		        		var start_date = myPoint.fn.leadingZeros(syyyy, 4) + myPoint.fn.leadingZeros(sm, 2) + myPoint.fn.leadingZeros(sd, 2);
		        		
		    		var param = {
		    				"user_ci"				: user_ci,
		    				"start_date"          : start_date,
		    				"end_date"          : end_date
		    		};
					var	opts = {
							url		: myPoint.functionURL3,
							data	: param,
							type	: "post",
							sendDataType	: "json",
							success	: function(resJSON, resCode) {
								console.log(resJSON);
								
								//페이징에 사용하기 위해 넣어줌 
								myPoint.initSaveList = resJSON.data.saveList;
								myPoint.initUseList = resJSON.data.useList;
								myPoint.saveListLength = resJSON.data.saveList.length;
								myPoint.useListLength = resJSON.data.useList.length;
								
								if (resCode== "success") {
									if(resJSON.data.resResult == "failure"){
//										alert("포인트 내역 조회중 오류가 발생하였습니다. \n 오른쪽 위 새로고침 버튼을 눌러 주세요.");
										// 180119 jyi 추가
								    		var _text = '포인트 내역 조회중 오류가 발생하였습니다. <br> 오른쪽 위 새로고침 버튼을 눌러 주세요.';
										$('#notice_word').empty();
										$('#notice_word').append(_text);
										$('#popup_type').val('default');
										$('#alert_type').hide();
										$('#alert_popup').show();
										// //180119 jyi 추가
									}

									myPoint.fn.drawHistoryList();
									
								}else{	
//									alert("오류가 발생하였습니다.");
									// 180119 jyi 추가
							    		var _text = '오류가 발생하였습니다.';
									$('#notice_word').empty();
									$('#notice_word').append(_text);
									$('#popup_type').val('default');
									$('#alert_type').hide();
									$('#alert_popup').show();
									// //180119 jyi 추가
								}
							}
							
						};
						common.http.ajax(opts);   		
		    	},	
				
		 }	
}

/* ----------------------------------공통-------------------------------------- */
function fnClear(obj){
	obj.value = '';
}


function inputNumberFormat(obj){
	obj.value = comma(uncomma(obj.value));
}
	

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

//콤마풀기
function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
    
}

function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}

function movePage(url, form){
	$("#"+form).attr("action",url);
	$("#"+form).submit();
	
}

