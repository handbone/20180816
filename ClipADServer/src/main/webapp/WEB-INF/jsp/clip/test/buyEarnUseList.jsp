<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- 이전 CLIP JS 및 CSS 파일
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
bx slider lib
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
-->


<!-- to-be -->
<!-- <link rel="stylesheet" href="../css/jquery.bxslider.css"/> -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
	
</script>
</head>
<body>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">쇼핑 적립 이용 내역</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<ul class="earn_tab tab_act">
			<li class="active">익월 적립</li>
			<li>익익월 적립</li>
		</ul>
		<!-- 익월적립 -->
		<div class="earn_tab_area tab_con">
			<ul class="earn_list">
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
			</ul>
			<div class="earn_nolist">
				최근 쇼핑 익월 적립 이용 내역이 없습니다.
			</div>
		</div>
		<!-- 익익월적립 -->
		<div class="earn_tab_area tab_con">
			<ul class="earn_list">
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
				<li>
					<div class="price">
						<span>G마켓</span>
						<em><strong>368,000</strong>원</em>
					</div>
					<div class="point">
						<span>2017.07.02</span>
						<em class="plus">+50,000<i>P</i></em>
					</div>
				</li>
			</ul>
			<div class="earn_nolist">
				최근 쇼핑 익익월 적립 이용 내역이 없습니다.
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
</body>
</html>