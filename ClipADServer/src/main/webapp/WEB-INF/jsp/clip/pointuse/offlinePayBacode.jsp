<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- to-be -->

<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">


</head>
<body>
<form id="form" name="form" action="offlinePayBacode.do" method="post">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	<input type="hidden"  id="item_id" name="item_id" value="${itemInfo.item_id}"/>
	<input type="hidden"  id="coupon_id" name="coupon_id" value="${param.coupon_id}" />
	<input type="hidden"  id="pin_id" name="pin_id" value=""/>
	
	<input type="hidden"  id="pageType" name="pageType" value="" />
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">매장 바로 쓰기</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back" ><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				오프라인 매장에서도 포인트로<br>
				알뜰하게 구매하세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="javascript:goSubmit()">
				<span class="title">클립포인트</span>
				<strong class="point usepointValue"><span id="mypoint" >0</span><span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<!-- 170721 수정 -->
		<div class="section">
			<div class="section_inner2">
				<div class="ad_area">
					<a href="javascript:;"><img src="${itemInfo.item_image_url}" alt=""></a>
				</div>
				<div class="barcode_area" id="barcode_area" style="display:block;">
					<span class="time">유효시간 <span id="timeDown">05:00</span><button type="button" onClick="javascript:getPinId(1);" >새로고침</button></span>
					<div class="section">
					    <div class="barcode_area">
					        <svg id="barcode"></svg>
					    </div>
					</div>
				</div>
			</div>
		</div>
		<div class="section">
			<div class="section_inner2">
				<h2 class="info_txt_title">이용안내</h2>
				<ul class="info_txt">
					
					<c:out value="${itemInfo.description}" escapeXml="false" />
				</ul>
			</div>
		</div>
	</div><!-- // contents -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!--  180119 popup 추가-->
	<%@include file="/WEB-INF/jsp/clip/alert_popup.jsp"%>
	<!--  //180119 popup 추가 -->
</div><!-- // wrap -->
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/pointCommon.js"></script>

<script src="../js/lib/JsBarcode.all.min.js"></script>

<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script type="text/javascript">
//180119 jyi 추가
function popup_confirm_Act(){
	var type = $('#popup_type').val();
	if(type == ''){
		$('#alert_popup').hide();
	}else{
		$('#alert_popup').hide();
	}
	
}
// //180119 jyi 추가
$(function() {
	 
	common.invoker.invoke("myPoint");
	chkGetBarCode('<c:out value="${param.coupon_id}" />'); 

});

var TimerStart = function() { tid = setInterval('msg_time()',1000); };
//최초 설정 시간(기본 05:00 초)
var SetTime = 299;
var getPinId = function(x){
	// [jaeheung] 2017.08.11 해당 div 존재 하면 삭제
	$('.barcode_re').remove();
	
	var paramData = {			
			"item_id" : $("#item_id").val(),
			"user_token" : $("#user_token").val()			
	};
	
	$.ajax({
    	url: "offlinePayPinId.do",
    	type: "POST",
        data: JSON.stringify(paramData),
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        success: function (resData) {
            
        	if(resData.itemInfo.couponId == null){
        		/* alert("바코드 발급이 실패하였습니다. 해당 장애가 지속적으로 발생시 고객센터로 문의 해주시기 바랍니다."); */
        		// 180119 jyi 추가
	    		var _text = '바코드 발급이 실패하였습니다. 해당 장애가 지속적으로 발생시 고객센터로 문의 해주시기 바랍니다.';
			$('#notice_word').empty();
			$('#notice_word').append(_text);
			$('#popup_type').val('default');
			$('#alert_type').hide();
			$('#alert_popup').show();
			// //180119 jyi 추가
        		$("#barcode_area").hide();
        		$(".btn_point_pay").show();
        		return;
        	}else{
        		
        		// [jaeheung] 2017.08.11 바코드 새로 고침 시 바코드 즉시 갱        		
        		if(x != null) { 
        			$('#timeDown').text('05:00');
	        		SetTime = 299;
	        		clearInterval(tid);
        		}
        		chkGetBarCode(resData.itemInfo.couponId); 
        	}            
        },
        beforeSend: function () {
        	loadingClip.openClip();
        },
        complete : function() {
        	loadingClip.closeClip();
        },
        error: function (e) {
            /* alert("네트워크 오류가 발생하였습니다."); */
            // 180119 jyi 추가
	    		var _text = '네트워크 오류가 발생하였습니다.';
			$('#notice_word').empty();
			$('#notice_word').append(_text);
			$('#popup_type').val('default');
			$('#alert_type').hide();
			$('#alert_popup').show();
			// //180119 jyi 추가
        }
    });
}

function chkGetBarCode(obj) {
	
	$("barcode_area").attr("class","barcode_area");
	
	JsBarcode("#barcode", obj, {
	      width: 4,
	      height: 200,
	      fontSize: 40,
	      textMargin: 25
	});
	
	TimerStart();
}

function msg_time() {	// 1초씩 카운트
	
	var mi = Math.floor(SetTime / 60);
	var hh = SetTime % 60;
	
	if(mi.toString().length < 2){
		mi = pad(mi, 2)
	}
	if(hh.toString().length < 2){
		hh = pad(hh, 2)
	}
	
	m = mi + ":" + hh ;	// 남은 시간 계산
	
	var msg = m;
	
	$("#timeDown").text(msg);	// div 영역에 보여줌 			
	SetTime--;					// 1초씩 감소	
	if (SetTime < 0) {			// 시간이 종료 되었으면..
		
		clearInterval(tid);		// 타이머 해제
		
		var addHTML = '<div class="barcode_re">';
		addHTML += '<div class="inner">';
		addHTML += '<p>유효시간이 초과되었습니다.</p>';
		addHTML += '<button type="button" onClick="javascript:getPinId(1);">재발행</button>';
		addHTML += '</div>';
		addHTML += '</div>';
		
		$('.barcode_re').remove();
		$('#barcode_area').append(addHTML);
	}	
}

function goSubmit() {
	$('#pageType').val('2');
	$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
}
</script>
</body>
</html>