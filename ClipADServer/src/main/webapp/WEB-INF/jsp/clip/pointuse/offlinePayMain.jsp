<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">


</head>
<body>
<form id="form" name="form" action="offlinePayDetail.do">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden"  id="item_id" name="item_id" value=""/>
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	
	<input type="hidden"  id="pageType" name="pageType" value="" />
</form>
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">매장 바로 쓰기</h1>
		<a href="javascript:goSubmit1();" class="btn_back" ><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				오프라인 매장에서도 포인트로<br>
				알뜰하게 구매하세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="javascript:goSubmit()">
				<span class="title">클립포인트</span>
				<strong class="point usepointValue"><span id="mypoint" >0</span><span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section">
			<div class="ad_area">
				<c:forEach var="list" items="${itemList}" > 
					<a href="javascript:goOfflinePayDetail(${list.item_id})"><img src="${list.comp_image_url}" alt=""></a>
				</c:forEach>
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/pointCommon.js"></script>

<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>

<script type="text/javascript">
$(function() {
	 
	common.invoker.invoke("myPoint");
	
});	

function goOfflinePayDetail(item_id){
	$("#item_id").val(item_id);
	$("#form").submit();
}
function goSubmit() {
	$('#pageType').val('2');
	$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
}
function goSubmit1() {
	$('#pageType').val('1');
	$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
}
</script>
</body>
</html>