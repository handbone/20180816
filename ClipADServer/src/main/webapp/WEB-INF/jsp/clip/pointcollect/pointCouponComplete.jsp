<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- as-is -->
<!-- 
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" href="../css/jquery.bxslider.css"/>
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>
-->

<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<form id="form" name="form" action="<c:url value="/pointmain/main.do" />">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden" id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden" id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden" id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden" id="mid" name="mid" value="${mid}"/>
	<input type="hidden" id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden" id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden" id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden" id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden" id="user_token" name="user_token" value="${user_token}"/>
	
	<input type="hidden" id="pageType" name="pageType" value="" />
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">포인트 쿠폰 등록</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="javascript:btnAct.redirect('0')">
				<!-- 170719 수정 -->
				<span class="title">클립포인트</span>
				<!-- // 170719 수정 -->
				<strong class="point"><span id="mypoint">0</span><span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section">
			<div class="section_inner">
				<!-- 포인트 전환 -->
				<div class="point_transform_wrap">
					<p class="top_title">
						<strong class="point"><!--
							--><fmt:parseNumber var="point" value="${coupon_value}" /><fmt:formatNumber value="${point}" pattern="#,###" /><span>P</span><!--
						--></strong>가 성공적으로<br>전환 되었습니다.
					</p>
					<a href="javascript:;" class="btn_point_more" onclick="javascript:btnAct.redirect('0')">포인트 더 모으러 가기!</a>
					<a href="javascript:;" class="btn_point_use" onclick="javascript:btnAct.redirect('1')">포인트 사용하러 가기!</a>
				</div>
				<!-- // 포인트 전환 -->
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script src="../js/common/jquery.json-2.4.js"></script>
<script src="../js/common/common.js"></script>
<script src="../js/my_point/pointCommon.js"></script>
<script>
	$(function(){
		common.invoker.invoke("myPoint");
	});
	
	btnAct = {
		redirect : function(pageType) {
			$('#pageType').val(pageType);
			$('#form').submit();
		}
	}
</script>
</body>
</html>