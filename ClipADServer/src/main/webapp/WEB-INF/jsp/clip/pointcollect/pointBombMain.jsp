<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>CLIP</title>
	
	<!-- 이전 CLIP JS 및 CSS 파일
	<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../js/common/common.js"></script>
	<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
	<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
	bx slider lib
	<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
	-->
	
	<!-- to-be  -->
	<link rel="stylesheet" href="../css/lib/swiper.min.css">
	<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<form id="form" name="form" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden" id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden" id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden" id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden" id="mid" name="mid" value="${mid}"/>
	<input type="hidden" id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden" id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden" id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden" id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden" id="user_token" name="user_token" value="${user_token}"/>
	
	<input type="hidden" id="level" name="level" value="" />
	<input type="hidden" id="point" name="point" value="" />
	<input type="hidden" id="base_point" name="base_point" value="" />
</form>
<!-- wrap -->
<div class="wrap noheader">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">랜덤 포인트 폭탄</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="bomb_wrap">
			<div class="bomb_top fixed_top">
				<img src="../images/bomb/bomb_top.png" class="img_top_txt" alt="3일마다 팡팡 터지는 랜덤 포인트 폭탄!">
			</div>
			<div class="bomb_touch">
				<a href="javascript:;">
					<!--  말풍선  -->
					<span class="bomb_sb">
						<img src="../images/bomb/bomb_touch1.png" alt="폭탄을 터치하여 터드리세요!">
					</span>
					<!--  // 말풍선  -->
					<span class="btn_touch">
						<img src="../images/bomb/bomb_touch2.png" alt="TOUCH!!">
					</span>
					<!--  폭탄  -->
					<span class="bomb">
						<img src="../images/bomb/bomb_touch3.png" alt="폭탄">
					</span>
					<!--  //폭탄  -->
					<!-- 폭탄 심지 -->
					<span class="bomb_wick">
						<img src="../images/bomb/bomb_touch6.png" alt="">
					</span>
					<span class="bomb_wick2">
						<img src="../images/bomb/bomb_touch5.png" alt="">
					</span>
					<!-- // 폭탄 심지 -->
					<!-- 폭탄 그림자 -->
					<span class="bomb_shadow">
						<img src="../images/bomb/bomb_touch_shadow.png" alt="">
					</span>
					<!-- // 폭탄 그림자 -->
				</a>
				<!-- 170726 수정 -->
				<div class="lv_txt">회원님의 폭탄레벨 : lv.<span id="txt_level"></span></div>
				<!-- // 170726 수정 -->
			</div>
			<div class="bomb_tip">
				<img src="../images/bomb/bomb_tip.jpg" alt="">
				<dl class="blind">
					<dt>폭탄레벨 올리는 꿀팁!</dt>
					<dd>잠금 화면을 풀 때, 왼쪽으로 슬라이드를 많이 할 수록 레벨 급상승!</dd>
					<dd>레벨이 오르면 최소 랜덤포인트도 함께 업!</dd>
				</dl>
			</div>
		</div>
	</div><!-- // contents -->
	<!-- 170728 추가 -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!-- //170728 추가 -->
	<!--  180119 popup 추가-->
	<%@include file="/WEB-INF/jsp/clip/alert_popup.jsp"%>
	<!--  //180119 popup 추가 -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script>
//180119 jyi 추가
function popup_confirm_Act(){
	var type = $('#popup_type').val();
	if(type == ''){
		$('#alert_popup').hide();
	}else{
		$('#alert_popup').hide();
	}
	
}
// //180119 jyi 추가
	$(function() {
	//	var result = '<c:out value="${result}" />';
/* 		if(result == 'F') {
			alert('<c:out value="${resultMsg}" />');
			window.history.back();
		} */
		
		$('.bomb_touch a').one('click', function(){
			fn.bomb.clickAct();
		});
		
		fn.join.bombJoin();
	});

	var fn = {
			custId : $('#cust_id').val(),
			trId : '<c:out value="${ pointBombInfo.tr_id }" />',
			join : {
				bombJoin : function() {
					var param = {
							'cust_id' : fn.custId,
							'tr_id' : fn.trId
					};
					
					$.ajax({
						type : 'POST',
						dataType : 'Json',
						url : '<c:url value="/pointbomb/pointBombJoin.do" />',
						data : param,
						success : function(resData) {
							console.log(resData);
							
							if(resData.result == 'F') {
								/* alert(resData.resultMsg); */
								// 180119 jyi 추가
						    		var _text = resData.resultMsg;
								$('#notice_word').empty();
								$('#notice_word').append(_text);
								$('#popup_type').val('default');
								$('#alert_type').hide();
								$('#alert_popup').show();
								// //180119 jyi 추가
								goBack();
							} 
							
							fn.trId= resData.pointBombInfo.tr_id;
							$('#txt_level').text(resData.pointBombInfo.level);
						},
						error : function(errData) {
							/* alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.'); */
							// 180119 jyi 추가
					    		var _text = '네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.';
							$('#notice_word').empty();
							$('#notice_word').append(_text);
							$('#popup_type').val('default');
							$('#alert_type').hide();
							$('#alert_popup').show();
							// //180119 jyi 추가
							console.log('================= ERROR =================');
							console.log(errData.statusText);
							console.log('=========================================');
						},
						beforeSend : function() {
							loadingClip.openClip();
						},
						complete : function() {
							loadingClip.closeClip();
						}
					});
				}
			},
			bomb : {
				clickAct : function() {
					var param = {
							'cust_id' : fn.custId,
							'tr_id' : fn.trId
					};
					
					$.ajax({
						type : 'POST',
						dataType : 'Json',
						url : '<c:url value="/pointbomb/pointBombComplete.do" />',
						data : param,
						success : function(resData) {
							console.log(resData);
							
							var pointBombInfo = resData.pointBombInfo;
							if(resData.result == 'S') {
								// fn.page.complete();
								$('#point').val(resData.reward_point);
								$('#level').val(resData.reward_level);
								$('#base_point').val(resData.reward_base_point);
								fn.page.complete();								
							}
							else {
								/* alert(resData.resultMsg); */
								// 180119 jyi 추가
						    		var _text = resData.resultMsg;
								$('#notice_word').empty();
								$('#notice_word').append(_text);
								$('#popup_type').val('default');
								$('#alert_type').hide();
								$('#alert_popup').show();
								// //180119 jyi 추가
							}
						},
						error : function(errData) {
							/* alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.'); */
							// 180119 jyi 추가
					    		var _text = '네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.';
							$('#notice_word').empty();
							$('#notice_word').append(_text);
							$('#popup_type').val('default');
							$('#alert_type').hide();
							$('#alert_popup').show();
							// //180119 jyi 추가
							console.log('================= ERROR =================');
							console.log(errData.statusText);
							console.log('=========================================');
						},
						beforeSend : function() {
							loadingClip.openClip();
						},
						complete : function() {
							loadingClip.closeClip();
						}
					});
				}
			},
			page : {
				main : function() {
					$('#form').attr('action', '<c:url value="/pointmain/main.do" />');
					$('#form').submit();
				},
				complete : function() {
					$('#form').attr('action', '<c:url value="/pointbomb/pointBombResult.do" />');
					$('#form').submit();
				}
			}
	}

 //    var boomMove ={
	//     init:function(){
	//         this.btn = $('.btn_touch');
	//         this.fire = $('.bomb_wick2');
	//         this.addEvent();
	//     },
	//     addEvent:function(){
	//         var _this = this;
	//         setInterval(function(){
	//         	if(!_this.btn.hasClass('active')){
	//         		_this.btn.addClass('active');
	//         	}else{
	//         		_this.btn.removeClass('active');
	//         	}	    		
	//         },500);

	//         setInterval(function(){
	//         	if(!_this.btn.hasClass('active')){
	//         		_this.fire.addClass('active');
	//         	}else{
	//         		_this.fire.removeClass('active');
	//         	}	    		
	//         },500);
	//     },
	//     txtMove:function(){
	    	
	    	
	    	
	//     }
	// }
	// boomMove.init(); 
</script>
</body>
</html>