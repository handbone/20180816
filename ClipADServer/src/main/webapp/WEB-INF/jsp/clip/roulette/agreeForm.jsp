<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
<meta name="HandheldFriendly" content="true" />
<meta name="MobileOptimized" content="320" />

<title>agreeForm</title> 

<link type="text/css" href="../css/roulette.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>

<!-- Contact Form CSS files -->
<link type='text/css' href='../css/agree_simplemodal.css' rel='stylesheet' media='screen' />

<script> 
$(document).ready(function() {
	
	
	 
	 if( "2" == "${rouletteDto.keytype}"){
		 $("#smsAuthDiv").hide();
	 }
	
	 $("#goInsert").bind("click",function() {
		 
		 if($("#agree_01").val() != 'Y'){
			 alert("개인정보 수집/이용동의는 필수 동의 항목입니다.");
			 return;
		 }
		 if($("#agree_02").val() != 'Y'){
			 alert("개인정보 제3자 제공동의는 필수 동의 항목입니다.");
			 return;
		 }
		 if($("#agree_01").val() == 'Y' && $("#agree_02").val() == 'Y'){
			 $("#status").val("Y");
		 }
		 
		 if( "2" == "${rouletteDto.keytype}"){
			 $("#agreeForm").submit();
		 }else if( "3" == "${rouletteDto.keytype}"){
			 
			if( $("#smscode").val() == ""){
				alert("인증번호를 입력해주세요.");
				return;
			}
			 
			formData = $("#agreeForm").serialize();
			$.ajax({
	            type: "POST",
	            url: "../roulette/smsAuthChk.do",
	            data: formData,
	            dataType: "Json",
	            success: function (resData) {
	                console.log(resData.result);  
	                if (resData.result == "success") {
	                	$("#agreeForm").submit();
	                } else {
	                	alert("인증번호가 유효하지 않습니다.");
	                }

	            },
	            error: function (e) {
	            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
	            }
	        });
		 }
		
		 
		 
	});
	 
	 $("#backBtn").bind("click",function() {
			if( "3" == $('#keytype').val() ){
				$(location).attr('href',$("#returnURL").val());
			}else{
				history.back();	
			}
		 	
		});
	 $("#backBtn2").bind("click",function() {
		 if( "3" == $('#keytype').val() ){
				$(location).attr('href',$("#returnURL").val());
			}else{
				history.back();	
			}
		}); 
	 $("#agree_check_01").bind("click",function() {
		if($("#agree_01").val() == 'N'){
			$(this).attr("src","../images/roulette/agree_check_01_0_${pointRoulette.imagepath}.png");
			$("#agree_01").val("Y");
		}else if($("#agree_01").val() == 'Y'){
			$(this).attr("src","../images/roulette/agree_check_01_${pointRoulette.imagepath}.png");
			$("#agree_01").val("N");
		}	
	});
	$("#agree_check_02").bind("click",function() {
		if($("#agree_02").val() == 'N'){
			$(this).attr("src","../images/roulette/agree_check_02_0_${pointRoulette.imagepath}.png");
			$("#agree_02").val("Y");
		}else if($("#agree_02").val() == 'Y'){
			$(this).attr("src","../images/roulette/agree_check_02_${pointRoulette.imagepath}.png");
			$("#agree_02").val("N");
		}	
	});
	
	
	/* $("#agree_btn_view_01").bind("click",function() {
		modalPopup("desc01");
	}); 
	$("#agree_btn_view_02").bind("click",function() {
		modalPopup("desc02");
	});  */
	$("#agree_btn_view_01").bind("click",function() {
		showView(1);
	}); 
	$("#agree_btn_view_02").bind("click",function() {
		showView(2);
	}); 
	
	
	function modalPopup(msg) {
		if( 'desc01' == msg ){
			$('#containerDesc01').modal({
				opacity:80,
				overlayCss: {backgroundColor:"#fff"}
			});
			return;
		}else if( 'desc02' == msg ){
			$('#containerDesc02').modal({
				opacity:80,
				overlayCss: {backgroundColor:"#fff"}
			});
			return;
		}
		
		
	}
	
	$("#send_btn").bind("click",function() { 
		
		 if( $("#phoneNumber").val() == ''){
	        alert("휴대폰 번호를 입력해 주십시오.");
	        return;
	     }
		 if( $("#phoneNumber").val().length < 10){
	        alert("잘못된 전화번호입니다.");
	        return;
	     }

		$("#tel").val($("#phoneNumber").val());
	    
		formData = $("#agreeForm").serialize();
		$.ajax({
            type: "POST",
            url: "../roulette/smsAuth.do",
            data: formData,
            dataType: "Json",
            success: function (resData) {
                console.log(resData.result);  
                if (resData.result == "success") {
                	alert("3분안에 인증번호를 입력해주세요.");
                } else {
                	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
                }

            },
            error: function (e) {
            	alert("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.");
            }
        });

	    
       
    });
	 
	var mobilecheck = function () {
    var check = false;
    (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
    }
    if(mobilecheck()){
        
    }else{
    	$("#titleDiv").hide();
    } 
	 
});

function maxLengthCheck(object) {
    if (object.value.length > object.maxLength) {
        object.value = object.value.slice(0, object.maxLength);
    }
}


/**이용약관 보기**/
var showView = function (num) {
    if ($("#detail" + num).css("display") == "none") {
        $("#detail" + num).show();
    } else {
        $("#detail" + num).hide();
    }
}

</script>
</head>
<body>
<form name="agreeForm" id="agreeForm" method="post"  action="<c:url value='/roulette/agreeInsert.do' />" >

<div style="text-align:center">
 
		<!-- 타이틀 start -->
       <table style="width:100%; cellspace:0px; cellpadding:0px; border-spacing: 0px;">
            <tr>
	            <td style="width:15%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/title_01_${pointRoulette.imagepath}.png" style="width:100%; " alt="뒤로가기" id="backBtn" /></td>
	            <td style="width:85%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/agree_title_${pointRoulette.imagepath}.png" style="width:100%; " alt="출석체크"/> </td>
	        </tr>
        </table>
        <%-- <table style="width:100%; cellspace:0px; cellpadding:0px; border-spacing: 0px;">
            <tr>
	            <td style="width:15%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/title_01_${pointRoulette.imagepath}.png" style="width:100%; " alt="뒤로가기" id="backBtn" /></td>
	            <td style="width:85%; vertical-align: bottom;  padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/agree_title_${pointRoulette.imagepath}.png" style="width:100%; " alt="출석체크"/> </td>
	        </tr>
        </table>
         --%>
        <!-- 타이틀 end -->
        <img src="../images/roulette/agree_top_bg_${pointRoulette.imagepath}.png" class="genie" alt="clip 매일매일 출석체크 이벤트!"/>
		<table>
		    <tr >
			    <td colspan="2" style="width:100%; padding:20px 20px 10px 20px;"><img src="../images/roulette/agree_txt_${pointRoulette.imagepath}.png" class="genie" alt="클립포인트 적립서비스 이용을 위해 아래의 동의가 필요합니다."/>
			    </td>
		    </tr>
		    <tr>
			    <td style="width:85%; padding:10px 10px 10px 20px;"><img id="agree_check_01" name="agree_check_01" src="../images/roulette/agree_check_01_${pointRoulette.imagepath}.png" class="genie" alt="개인정보 수집/이용동의(필수)"/>
			    </td>
			    <td style="width:15%; padding:10px 20px 10px 10px;"><img id="agree_btn_view_01" name="agree_btn_view_01" src="../images/roulette/agree_btn_view_${pointRoulette.imagepath}.png" class="genie" alt="보기"/>
			    </td>
		    </tr>
		  </table> 
		    <div id="detail1" class="table_style1"  style="display:none; " >
							<p><font color="red">개인정보 수집/이용동의(필수)</font></p>
							${pointRoulette.desc01}
		    </div>
		  <table>
		    <tr>
			    <td style="width:85%; padding:0px 10px 20px 20px;"><img id="agree_check_02" name="agree_check_02" src="../images/roulette/agree_check_02_${pointRoulette.imagepath}.png" class="genie" alt="개인정보 제 3자제공동의(필수)"/>
			    </td>
			    <td style="width:15%; padding:0px 20px 20px 10px;"><img id="agree_btn_view_02" name="agree_btn_view_02" src="../images/roulette/agree_btn_view_${pointRoulette.imagepath}.png" class="genie" alt="보기"/>
			    </td>
		    </tr>
		  </table>
		    <div id="detail2" class="table_style1" style="display:none; " >
							<p><font color="red">개인정보 제 3자제공동의</font></p>
							${pointRoulette.desc02}
		    </div>
		     <!--본인인증 확인 start-->
		   <table id="smsAuthDiv" style="width:100%; padding:20px 20px 20px 20px ;">
		    <tr>
			    <td style="width:100%; padding-bottom:10px;">
				    <input type="text" name="phoneNumber" id="phoneNumber" style="width:70%; height: 40px; " placeholder="  휴대폰번호 입력" maxlength="11" oninput="maxLengthCheck(this)" value="01028615896"/>
					<input type="button" id="send_btn" name="send_btn" style="height: 40px; " value="인증번호 전송">
				</td>
			</tr>	
			<tr>
				<td>
				<input type="text" id="smscode" name="smscode" style="width:100%; height: 40px; " placeholder="  인증번호 입력 (3분 이내)" /> 
				</td>
			</tr>
			</table>
			<!--본인인증 확인 end-->
			<table>
			<tr>
		    	<td colspan="2">
		     		<table style="width:100%; ">
			            <tr>
				            <td style="width:50%; padding:10px 5px 10px 20px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img id="backBtn2" src="../images/roulette/agree_btn_cancel_${pointRoulette.imagepath}.png" style="width:100%; " alt="취소" /></td>
			             	<td style="width:50%; padding:10px 20px 10px 5px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img id="goInsert" name="goInsert" src="../images/roulette/agree_btn_submit_${pointRoulette.imagepath}.png" style="width:100%; " alt="확인"/> </td>
			             </tr>
			        </table>
			     </td>
			 </tr>
		</table>
	</div>


  
	
	

<div  id='containerDesc01' style="display:none; padding: 10 10 10 10;" >
	<div style=" font-size:13px; text-align:center;">${pointRoulette.desc01}</div> 
</div>

<div  id='containerDesc02' style="display:none; padding: 10 10 10 10;" >
	<div style=" font-size:13px; text-align:center;">${pointRoulette.desc02}</div> 
</div>


	<input type="hidden" name="roulette_id" id="roulette_id" value="${rouletteDto.roulette_id}"/>
	<input type="hidden" name="keyid" id="keyid" value="${rouletteDto.keyid}"/>
	<input type="hidden" name="keytype" id="keytype" value="${rouletteDto.keytype}"/>
	<input type="hidden" name="returnPage" id="returnPage" value="${rouletteDto.returnPage}"/>
	<input type="hidden" name="returnURL" id="returnURL" value="${rouletteDto.returnURL}"/>
	<input type="hidden" id="status" name="status" value="" >
	<input type="hidden" id="agree_01" name="agree_01" value="N" >
	<input type="hidden" id="agree_02" name="agree_02" value="N" >
	<input type="hidden" id="tel" name="tel" value="" >
</form>

  
  <!-- Load jQuery, SimpleModal and Basic JS files -->
<script type='text/javascript' src='../js/common/jquery.simplemodal.js'></script>
<script type='text/javascript' src='../js/common/basic.simplemodal.js'></script>

</body>
</html>
